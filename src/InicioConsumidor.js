import React, { Component } from "react";
import {Link} from "react-router-dom";
import { Button} from 'antd';
// import banner from './images/banner.png';
// import premio4 from './images/premio-23.png';
// import premio5 from './images/premio-24.png';
// import premio6 from './images/premio-25.png';
// import premio1 from './images/premio-20.png';
// import premio2 from './images/premio-21.png';
import homeBanner from './images/banner_home.png';
import banner_medio from "./images/Asset6.png";
import TagManager from 'react-gtm-module'
import actions from './redux/auth/actions';
import { connect } from 'react-redux';
const {changePage} = actions;

class Inicio extends Component {
constructor(props){
  super(props);
}
  componentDidMount(){
    //this.props.location.aboutProps.setState({tittle:"PREMIOS"});
    const tagManagerArgs = {
      gtmId: 'GTM-WSJ4CPF',
      dataLayer:{
        event: 'pageview',
        /* pageTitle: 'Premios', */
      }
    }
    TagManager.initialize(tagManagerArgs)
    console.log("inicia Tag manager");
    window.gtag('event', 'conversion', {
      'allow_custom_scripts': true,
      'send_to': 'DC-9238337/mdlzt00/nuevo000+standard'
    });
  
    /* this.props.changePage("PARTICIPACIÓN") */
  }


  render() {
    return (
      <>
    <div className="container containerInside">

        <div className="container-fluid">
          <div className="row justify-content-md-center galleria-home full-w">
            <div style={{textAlign: 'center'}}>
              <img src={banner_medio} className="cookieHome" />
            </div>
          </div>
        </div>
     {/*  <div>
         <div className="imgBoxHomeBanner" style={{maxWidth: 460}}>
            <img className="imgBtnMenu participarImg" alt="homeBanner" src={homeBanner}/>
          </div>
      </div> */}
      
      {/* <div className="row row-center pt-2">
          <div className="whiteBox">
              <img src={cinta} className="cintaimg" style={{maxWidth:85}}/>
            <div className="whiteSquare participar">
              <div className="roaster red tit">PREMIACIÓN</div>
            </div>
        </div>
      </div>
      <h1 className="roaster yellow lineheight" style={{marginTop:20}}>
        participá en el sorteo final
      </h1> */}
            {/* <div className="row-center second pt-2">
               <div className="textPremios">
                    ¡y además podés ganar increíbles premios!
                </div>
            </div> */}

            {/* <div className="row pt-2 row-center row-premios">
              <div className="col-md-4 col-6">
                <div className="imgBoxAward">
                <img className="lettering premioImg" src={premio1}/>                                  
                </div>
                <div className="awardName">
                gidftcards 
                  <br></br>
                $ 5.000
                </div>
              </div>
              <div className="col-md-4 col-6">
                <div className="imgBoxAward">
                <img className="lettering premioImg" src={premio2}/>
                </div>
                <div className="awardName">
                  gidftcards  
                  <br></br>                    
                  $ 10.000
                </div>
              </div>
                            <div className="col-md-4 col-6">
                              <div className="imgBoxAward sco">
                <img className="lettering premioImg" src={premio3}/>
                </div>
                <div className="awardName">
                  monopatines 
                  <br></br>
                  eléctricos
                </div>
              </div>
                            <div className="col-md-4 col-6">
                              <div className="imgBoxAward">
                <img className="lettering premioImg" src={premio4}/>
                </div>
                <div className="awardName">
                  notebooks
                </div>
              </div>
                            <div className="col-md-4 col-6">
                              <div className="imgBoxAward tab">
                <img className="lettering premioImg" src={premio5}/>
                </div>
                <div className="awardName">
                  tablets
                </div>
              </div>
                            <div className="col-md-4 col-6">
                              <div className="imgBoxAward bag">
                <img className="lettering premioImg" src={premio6}/>
                </div>
                <div className="awardName">
                  mochilas 
                  <br></br>
                  con producto
                </div>
              </div>
            </div> */}
  {/* <div className="container-fluid">
          <div className="row justify-content-md-center galleria-home full-w ">
         
            <div className="box-img-cookiess small">
                <img src={cookies} className="card-img-top small"/>
            </div> 
            <div className="box-img-cookiess">
                <img src={cookies} className="cookieHome"/>
            </div>
          </div>
        </div> */}


        {/* <div className="homeMenuBtn " style={{marginTop: 20}}>
        <Link to="/participar/registro">
          <Button type="primary" className="botonPrimario">PARTICIPAR</Button>
        </Link>
      </div> */}
    </div>

</>
    );
  }
}

export default connect(
  /* state => ({}),
  {changePage} */
)(Inicio);