import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter, Link } from "react-router-dom";
import { Button, Col, Row } from "antd";
import TagManager from "react-gtm-module";
import actions from "../redux/auth/actions";
import yaestas from "../images/yaestas-09.png";
import ysorteos from "../images/ysorteos-11.png";
import kangoo from "../images/Camioneta.png";
const { changePage } = actions;

class GanadorComercialComponent extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    //this.props.history.push("/participar/codigo/ganador");
    const tagManagerArgs = {
      gtmId: "GTM-WSJ4CPF",
      dataLayer: {
        event: "pageview",
        pageTitle: "Ganador",
      },
    };
    TagManager.initialize(tagManagerArgs);
    this.props.changePage("PARTICIPACION");
  }

  render() {
    return (
      <div className="container">
        <div className="card my-5 card-white" id="cardGanadorComercial">
          <div className="container row" style={{ padding: "20px 0" }}>
            <Row>
              <div className="contenedorGanadorComercial">
                <Col xs={10}>
                  <img className="img-p" src={yaestas} />
                </Col>
                <Col xs={11}>
                  <img className="img-p" src={kangoo} />
                </Col>
              </div>
            </Row>
            <Row>
              <div className="contenedorGanadorComercial">
                <Col
                  xs={15}
                  sm={15}
                  md={15}
                  lg={15}
                  style={{ marginTop: "15px", marginBottom: 20 }}
                >
                  <img className="img-p " src={ysorteos} />
                </Col>
              </div>
            </Row>
            </div>
            {/* <p className="resp txt-dato white-txt int">
          Y ADEMÁS YA ESTÁS PARTICIPANDO EN EL SORTEO FINAL POR $100.000
          </p> */}

            {/* <p className="txt-flag my-3">
          <span className="bluetxt">COMUNÍCATE AL 011-5789-7132</span> <br></br>
          PARA COORDINAR LA ENTREGA DEL PREMIO
        </p> */}
            <hr style={{ borderTop: "2px dotted #000" }}></hr>

            <div className="centrado btnes   btnsMsg">
              <div>
                <Link to={"/participar/foto"}>
                  <Button className="botonPrimario bts" type="primary">
                    CARGAR NUEVA FACTURA
                  </Button>
                </Link>
              </div>
              <div style={{ marginTop: 20, marginBottom: 20 }}>
                <Link to={"/"}>
                  <Button className="botonPrimario bts" type="primary">
                    IR AL INICIO
                  </Button>
                </Link>
              </div>
            </div>
          </div>
        </div>
      
    );
  }
}

//export default withRouter(GanadorComponent);
const GanadorRouter = withRouter(GanadorComercialComponent);
export default connect(
  (state) => ({
    profile: state.Auth.profile,
  }),
  { changePage }
)(GanadorRouter);
