import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect, Link } from "react-router-dom";
import {
  Form,
  Input,
  Modal,
  Button,
  Label,
  Alert,
  message,
  Select,
  Checkbox,
  DatePicker,
} from "antd";
import ReactDOM from "react-dom";
import barraDividir from "../images/puntos_separador.png";
import actions from "../redux/auth/actions";

import RestClient from "../network/restClient";
import moment from "moment";

const Option = Select.Option;
const { login, changePage } = actions;

class RegistroFormComercial extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      error: undefined,
      provincias: [],
    };
  }

  componentDidMount() {
    this.loadProvincias();
    this.props.changePage("Participación");
  }

  loadProvincias = () => {
    RestClient.getProvincias().then((response) => {
      this.setState({ provincias: response.data.provincias });
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();

    this.props.form.validateFields((err, values) => {
      if (values.instagram == null) {
        values.instagram = " ";
      }
      if (!err) {
        this.setState({ loading: true, error: undefined });
        let registroObj = {
          ...values,
          fecha_nacimiento: moment(
            values.fecha_nacimiento,
            "DD-MM-YYYY"
          ).format("YYYY-MM-DD"),
        };
        RestClient.register(registroObj)
          .then((usuarioData) => {
            this.setState({ loading: false, error: undefined });
            this.props.login(usuarioData.data.user);
          })
          .catch((error) => {
            this.setState({ loading: false, error: error.message });
          });
      }
    });
  };

  render() {
    const { token } = this.props.profile ? this.props.profile : {};
    if (token) {
      return <Redirect to={"/participar/foto"} />;
    }

    const dateFormat = "DD/MM/YYYY";

    const { getFieldDecorator } = this.props.form;

    const config = {
      rules: [{ required: true, message: "Selecciona una fecha" }],
    };

    const validateStrings = (rule, value, callback) => {
      if (!/^[a-zñÑáéíóúÁÉÍÓÚ ]+$/i.test(value)) {
        callback("Sólo letras");
      } else {
        callback();
      }
    };

    const validateNumbers = (rule, value, callback) => {
      if (!/^[0-9]*$/i.test(value)) {
        callback("Sólo números");
      } else {
        callback();
      }
    };

    const validateDNI = (rule, value, callback) => {
      if (!/^[0-9]*$/i.test(value)) {
        callback("El DNI es inválido");
      } else {
        callback();
      }
    };

    const validatePassword = (rule, value, callback) => {
      if (
        !/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/.test(value) ||
        value.length < 10
      ) {
        callback("Password inválido");
      } else {
        callback();
      }
    };

    return (
      <div className="container  ">
        <Form
          onSubmit={this.handleSubmit}
          style={{ fontFamily: "FatFrankRegular" }}
        >
          <p className="resp txt-dato reg-tit" style={{ marginBottom: 0 }}>
            {/* <div className="number numberCenter"></div> */}Cargá tus datos
          </p>
          <div className="row" style={{ marginTop: 20 }}>
            <div className="col-24 col-md-6">
              <Form.Item label="Nombre del comercio:">
                {getFieldDecorator("nameComercio", {
                  rules: [
                    {
                      required: true,
                      message: "Por favor ingresa el nombre del comercio",
                    },
                    { validator: validateStrings },
                  ],
                })(<Input className="success" />)}
              </Form.Item>
            </div>
            <div className="col-24 col-md-6">
              <Form.Item label="CUIT/CUIL:">
                {getFieldDecorator("datoPers", {
                  rules: [
                    {
                      required: true,
                      message: "Por favor ingresa CUIL o CUIT",
                    },
                    { validator: validateNumbers },
                  ],
                })(<Input className="success" />)}
              </Form.Item>
            </div>
            <hr style={{ borderTop: "2px dotted   #fca10a" }}></hr>
            <div className="col-24 col-md-6">
              <Form.Item label="Nombre:">
                {getFieldDecorator("name", {
                  rules: [
                    { required: true, message: "Por favor ingresa tu nombre" },
                    { validator: validateStrings },
                  ],
                })(<Input className="success" />)}
              </Form.Item>
            </div>
            <div className=" col-24 col-md-6">
              <Form.Item label="Apellido:">
                {getFieldDecorator("apellido", {
                  rules: [
                    {
                      required: true,
                      message: "Por favor ingresa tu apellido",
                    },
                    { validator: validateStrings },
                  ],
                })(<Input className="success" />)}
              </Form.Item>
            </div>

            <div className=" col-24 col-md-6">
              <Form.Item label="Celular:">
                {getFieldDecorator("telefono", {
                  rules: [
                    {
                      required: true,
                      message: "Por favor ingresa tu teléfono",
                    },
                    { validator: validateNumbers },
                  ],
                })(<Input className="success" />)}
              </Form.Item>
            </div>

            <div className=" col-24 col-md-6">
              <Form.Item label="Provincia:" className="selected">
                {getFieldDecorator("provincia", {
                  rules: [
                    {
                      required: true,
                      message: "Por favor selecciona una provincia",
                    },
                  ],
                })(
                  <Select
                    className="success"
                    placeholder="Seleccione una provincia"
                    onChange={this.handleSelectChange}
                  >
                    {this.state.provincias.map((provincia) => (
                      <Option
                        style={{ fontFamily: "FatFrankRegular" }}
                        key={`provincia-${provincia.id_provincia}`}
                        value={provincia.id_provincia}
                      >
                        {provincia.provincia}
                      </Option>
                    ))}
                  </Select>
                )}
              </Form.Item>
            </div>

            <div className=" col-24 col-md-6">
              <Form.Item label="DNI:">
                {getFieldDecorator("dni", {
                  rules: [
                    { required: true, message: "Campo obligatorio:" },
                    { validator: validateDNI },
                  ],
                })(<Input className="success" />)}
              </Form.Item>
            </div>

            <div className=" col-24 col-md-6">
              <Form.Item label="Mail:">
                {getFieldDecorator("email", {
                  rules: [
                    { type: "email", message: "El correo no es válido" },
                    { required: true, message: "Por favor ingresa tu correo" },
                  ],
                })(<Input className="success" />)}
              </Form.Item>
            </div>
          </div>

          <div className="row">
            <div className="col-md-6 col-xs-12 col-sm-12 pb-3 checkboxReg">
              <Form.Item className="ch">
                {getFieldDecorator("mayor", {
                  valuePropName: "checked",
                  rules: [
                    { required: true, message: "Debe ser mayor de 18 años" },
                  ],
                })(
                  <Checkbox style={{ fontSize: 15 }}>
                    Soy mayor de 18 años
                  </Checkbox>
                )}
              </Form.Item>
              <Form.Item className="ch">
                {getFieldDecorator("agreement", {
                  valuePropName: "checked",
                  rules: [
                    {
                      required: true,
                      message: "Debe aceptar los términos y condiciones",
                    },
                  ],
                })(
                  <Checkbox style={{ fontSize: 15 }}>
                    Acepto{" "}
                    <Link to={"/bases-y-condiciones-comercial"} className="term">
                      Bases y Condiciones
                    </Link>
                  </Checkbox>
                )}
              </Form.Item>
            </div>
          </div>
          {this.state.error && this.state.error !== '' && 
              <div style={{marginBottom: 20}}>
                <Alert message={this.state.error} type="error" />
              </div>}
          <div style={{ textAlign: "center", paddingBottom: 30 }}>
            <Button
              loading={this.state.loading}
              type="primary"
              className="botonPrimario btnRegistro"
              htmlType="submit"
            >
              SIGUIENTE
            </Button>
          </div>
        </Form>
      </div>
    );
  }
}
const RegistroFormWrapped = Form.create({ name: "registroFormComercial" })(
  RegistroFormComercial
);
export default connect(
  (state) => ({
    profile: state.Auth.profile,
  }),
  { login, changePage }
)(RegistroFormWrapped);
