import React, { Component } from "react";
import MasonryLayout from 'react-masonry-layout';
import milk from './images/milk.jpg';
import cheese from './images/cheese.jpg';
import brandImage from './images/brand-08.png';
import postre2 from './images/postre2.jpg';
import postre3 from './images/postre3.jpg';
import postre4 from './images/postre4.jpg';
import postre5 from './images/postre5.jpg';
import postre6 from './images/postre6.jpg';
import postre7 from './images/postre7.jpg';
import postre8 from './images/postre8.jpg';
import instafooter from './images/instafooter.png';
import objectFitImages from 'object-fit-images';
import cookies from './images/terra-03.png';


import RestClient from './network/restClient';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import TagManager from 'react-gtm-module'
import actions from './redux/auth/actions';
import { connect } from 'react-redux';
const {changePage} = actions;

class Recetas extends Component {

  constructor(props){
    super(props);
    this.state={
      loadingRecetas:false,
      recetas:[]
    }
  }
  componentDidMount(){
    this.loadRecetas();
    const tagManagerArgs = {
      gtmId: 'GTM-WSJ4CPF',
      dataLayer:{
        event: 'pageview',
        pageTitle: 'Recetas',
      }
    }
    TagManager.initialize(tagManagerArgs);
    window.gtag('event', 'conversion', {
      'allow_custom_scripts': true,
      'send_to': 'DC-9238337/mdlzt00/nuevo002+standard'
    });
  
    this.props.changePage("mural")
  }


  loadRecetas = ()=>{
      RestClient.listRecetasAll().then(recetasObj=>{
        let recetas = recetasObj.data.recetas;
        recetas.map(receta=>{
          receta.key= `${receta.id_receta}-receta`;
        })
        this.setState({recetas},()=>{
          objectFitImages();
        });
      })
  }

  render() {
    let patronNormal = true;
    return (
    <div className="container">




      <div className="justify-content-md-center gridTerra" style={{position:'relative', paddingTop:50}} >
        <MasonryLayout
          id="masonry-layout"
          sizes={[
              { columns: 1, gutter: 0 },
              { mq: '500px', columns: 2, gutter: 0 },
              { mq: '768px', columns: 3, gutter: 0 },
              { mq: '1024px', columns: 4, gutter: 0 }
            ]}
          position={true}
          >
          {this.state.recetas.map((receta, i) => {
            let height = 0;
            let heightClass = '';
            if(i%4 === 0 && i!=0 ){
              patronNormal = !patronNormal;
            }
            if(patronNormal){
              height = i % 2 === 0 ? 236 : 336;
              heightClass = i % 2 === 0 ? 'contenedorImagenRecetaSmall' : 'contenedorImagenRecetaBig';
            }else{
              height = (i % 4 === 0 || i % 4 === 1)? 336 : 236;
              heightClass = (i % 4 === 0 || i % 4 === 1)? 'contenedorImagenRecetaBig' : 'contenedorImagenRecetaSmall';
            }
            return (
              <div
                key={i}
                className="picsMason animation all 2"
                style={{
                  display: 'block',
                }}>
                <div className="card">
                  <div className={heightClass}>
                    <LazyLoadImage
                      alt={""}
                      height={height}
                      effect="blur"
                      className="card-img-top galeria"
                      src={receta.foto}/>
                  </div>
                </div>
              </div>
            )}
          )}

        </MasonryLayout>
      </div>

      {/* <div className="container-fluid">
          <div className="row justify-content-md-center galleria-home full-w small">

            <div className="box-img-cookiess small">
                <img src={cookies} className="card-img-top small"/>
            </div>

          </div>
    </div> */}

    </div>
    );
  }
}

export default connect(
  state => ({}),
  {changePage}
)(Recetas);
