import React, { Component } from "react";
import { connect } from 'react-redux';
import {
  withRouter
} from "react-router-dom";
import { Form, Input, Modal, Button, Label,Row, Col,message,Upload,Icon,Progress } from 'antd';
import barraDividir from '../images/puntos_separador.png'
import TagManager from 'react-gtm-module'
import cookies from '../images/terra-03.png';
import fb from '../images/redess-28.png';
import insta from '../images/redess-29.png';
import tw from '../images/redess-27.png';
import CompartirPost from './comparti';

import RestClient from '../network/restClient';



const { Dragger } = Upload;


const props = {
  name: 'file',
  multiple: true,
  action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
  onChange(info) {
    const { status } = info.file;
    if (status !== 'uploading') {
      console.log(info.file, info.fileList);
    }
    if (status === 'done') {
      message.success(`${info.file.name} file uploaded successfully.`);
    } else if (status === 'error') {
      message.error(`${info.file.name} file upload failed.`);
    }
  },
};

class RedesComponent extends Component {

  constructor(props) {
    super(props);

    this.state = {
      error:undefined,
      loading: false,
      file: '',
      imagePreviewUrl: undefined,
      progreso:0
    }

  }
 
  componentDidMount(){
    this.props.history.push("/participar/foto");
  }


  handleUploadSubmit = (e)=>{
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
       if (!err) {
         let fotoObj = {
           image: values.image.file.name,
           receta: values.receta
         }
         this.setState({loading:true,error:undefined});
         RestClient.preSignedUrl({filename:fotoObj.image}).then(urlObj=>{
           let url = urlObj.data.url;
           const formData = new FormData();
           const file = this.state.fileList[0];
           formData.append('file', file);
           this.uploadFileToS3(formData,url,file.name,file,fotoObj);
         }).catch(error=>{
           this.setState({loading:false,error:error.message});
         })
       }
     });

  }

  uploadFileToS3(postData,url,filename,file,postParams){
      var xhr = new XMLHttpRequest();
      var secuencia=0;
      xhr.open("PUT", url);
      xhr.onreadystatechange = function() {
        if(xhr.readyState === 4){
          if(xhr.status === 200 || xhr.status === 204){
            this.saveRecerta(postParams);
          }else{
            this.setState({loading:false,error:"Error al subir la foto en el servidor"});
          }
       }
      }.bind(this);
      xhr.upload.onprogress = function(evt){
        var percentComplete = (evt.loaded / evt.total) * 100;
        this.setState({progreso:parseInt(percentComplete)});
      }.bind(this);
    xhr.send(file);
  }

  saveRecerta = (params)=>{
    RestClient.guardarReceta(params).then(guardado=>{
      this.setState({loading:false,error:undefined});
     // this.props.redesHandler()
    }).catch(error=>{
        this.setState({loading:false,error:error.message});
    })
  }

 getBase64 = (img, callback)=> {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  }
  beforeUpload = (file) => {

    const isValid = (file.type === 'image/jpeg' || file.type === 'image/png');
    if (!isValid) {
      message.error('Solo se puede subir imágenes');
    }
    const isLt2M = file.size / 1024 / 1024 < 10;
    if (!isLt2M) {
      message.error('La imagen no puede pesar más de 10 mb');
    }
    if (isValid && isLt2M){
      this.getBase64(file, (imageUrl) => {
        file.thumbUrl = imageUrl;
        this.setState({fileList: [file],
          imagePreviewUrl: imageUrl
        });
      });
    }
    return false;
  }


  render() {
    const { getFieldDecorator } = this.props.form;
  //  const url_del_post="https://terrabusi2020.s3.amazonaws.com/post/1579833235609_WhatsApp%20Image%202020-01-23%20at%2009.13.57.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAXMTCTDK5XHZPI34X%2F20200128%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20200128T011959Z&X-Amz-Expires=172800&X-Amz-Signature=06a87fe4cedd03578b88aa0169aa14677b43ea50de4de0d78d492866f500cdba&X-Amz-SignedHeaders=host";
    const url_del_post=this.props.state.urlImagen;
    console.log(this.props.state);
    const titulo_del_post="Participando en Tang";
    const resumen_del_post="https://www.promociontang.com.ar/";
    return (
      <div className="container fotoContainer containerInside">
        <div className="row row-center">
          

        <div className="col-lg-8">
            <p className="resp txt-dato strech1">
                         
        

            </p>
            <p className="resp txt-dato titCod" style={{marginTop:10, marginBottom:20}}>
              <div className="number numberCenter">4.</div>
              Compartí en redes sociales<br/>y TRIPLICÁ TUS CHANCES
            </p>
            {/* <div style={{textAlign:'center'}}><img style={{marginTop:15, maxWidth:230, marginBottom:20}} src={barraDividir} className="barraDividir"/></div> */}


            <div className="caja-b pt-3 foto-box">
                <div className="form-group caja-img">
                    <div className="input-group borde-blanco">
                        <span className="input-group-btn left-browser-foto">
                          <div className="foto-preview-compartir">
                            <img src={this.props.state.imagePreviewUrl}></img>
                          </div>
                        </span>
                    </div>
                  </div>
              <div className="inputUploadFile">
                <div className="uploadFileName">
                  <span className="fileNameTxtUpload" style={{fontSize:22}}>COMPARTIR EN</span>
                  <CompartirPost Url={url_del_post} Titulo={titulo_del_post} Resumen={resumen_del_post} />
                </div>
              </div>
            </div>
            


            <Form onSubmit={this.props.redesHandler}>
              <div className="centrado pt-3 progressUpload foto-box">
                <Row gutter={20}>
                  <Col xs={12}><Button  onClick={()=> this.props.redesHandler()} className="botonPrimario botonSecundario boton100" type="primary">omitir</Button></Col>
                  <Col xs={12}><Button loading={this.state.loading} htmlType="submit"  className="botonPrimario boton100" type="primary">Continuar</Button></Col>
                </Row>
                {this.state.loading && <div className="contenedorProgress"><Progress percent={this.state.progreso} /></div>}
              </div>
            </Form>

        </div>
      </div>


      </div>
    );
  }
}

const RedesWraper = Form.create({ name: 'foto' })(RedesComponent);
const RedesRouter =  withRouter(RedesWraper);
export default connect(
  state => ({
    profile: state.Auth.profile,
  }),
  {}
)(RedesRouter);
