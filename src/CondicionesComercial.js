import React, { Component } from "react";
import TagManager from "react-gtm-module";
import cinta from "./images/cinta-19.png";
import actions from "./redux/auth/actions";
import { connect } from "react-redux";
const { changePage } = actions;
class Condiciones extends Component {
  componentDidMount() {
    const tagManagerArgs = {
      gtmId: "GTM-WSJ4CPF",
      dataLayer: {
        event: "pageview",
      },
    };
    TagManager.initialize(tagManagerArgs);
    this.props.changePage("bases y condiciones");
  }

  render() {
    return (
      <div className="container conds containerInside" style={{margin:"50px auto",backgroundColor:"#f39906"}}>
        <div>
          <div class="col-12">
            <div class="conditions-description">
              1. Participan en la promoción denominada “PROMO EL SABOR DE GANAR
              | WEB | MAYORISTAS” (la “Promoción”), organizada por Mondelez
              Argentina S.A. (el “Organizador”), los polvos para preparar
              bebidas comercializados por el Organizador bajo las marcas “Tang”,
              “Clight” y “Verao”, en todos sus sabores y tamaños de envase
              (“Producto/s Participante/s”), obre o no en sus envases
              (“Envase/s”) publicidad de la Promoción.
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
              2. La Promoción tendrá vigencia en comercios mayoristas adheridos
              de la República Argentina (“Mayorista/s Adherido/s”), excepto en
              las provincias de Neuquén, Río Negro y Tierra del Fuego, Antártida
              e Islas del Atlántico Sur (“Territorio”), desde el 15 de enero de
              2022 hasta el 15 de marzo de 2022 (el “Plazo de Vigencia”) y se
              regirá por lo establecido en estas bases y condiciones (las
              “Bases”).
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
              3. Podrán participar de la Promoción las personas humanas mayores
              de 18 años y las personas jurídicas, en ambos casos, que sean
              titulares de la habilitación municipal de comercios minoristas
              autorizados para la venta de alimentos y bebidas ubicados en el
              Territorio (“Comercio/s Minorista/s”).
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
              4. La compra de cada cinco (5) displays de: veinte (20) Envases de
              “Tang” de 18 gramos o de veinte (20) Envases de “Clight” de 8
              gramos sabor POMELO ROSADO Vitamina C+D o sabor LIMONADA Vitamina
              C+D o sabor POMELO AMARILLO Vitamina C+D o sabor MANDARINA
              Vitamina C+D o de 7,5 gramos sabor NARANJA DURAZNO Vitamina C+D o
              sabor MANZANA VERDE Vitamina C+D o de 7 gramos sabor ANANÁ
              Vitamina C+D o sabor NARANJA MANGO Vitamina C+D o sabor PERA
              Vitamina C+D o de 16X16X7.5G sabor LIMÓN STEVIA Vitamina C+D o de
              16X16X9.5G sabor NARANJA STEVIA Vitamina C+D o de 16X16X7.5G LIMÓN
              STEVIA o de veinte (20) Envases de “Verao” de 10 gramos
              (indistintamente, “Displays”) o de cinco (5) Displays de cualquier
              combinación de los Productos Participantes, realizada en una misma
              oportunidad, por un Comercio Minorista, durante el Plazo de
              Vigencia, en cualquiera de los Mayoristas Adheridos a la Promoción
              (“Compra/s”), habilitará al Comerio Minorista que haya efectuado
              la Compra a participar de la Promoción. Para ello deberá, durante
              el Plazo de Vigencia, ingresar en el sitio Web
              www.elsabordeganar.com.ar (el “Web Site”) y, siguiendo las
              instrucciones allí indicadas, (i) Completar su nombre y apellido,
              D.N.I., dirección de e-mail, número de teléfono celular, provincia
              en la que resida y su número de CUIT, y el nombre y domicilio de
              su Comercio Minorista (“Datos Personales”) y (ii) Subir una imagen
              de la factura o ticket de la Compra (indistintamente “Factura/s” y
              juntamente con los Datos Personales, los “Datos”) en la que pueda
              verse con claridad el detalle de los Productos Participantes
              adquiridos. Asimismo, deberán aceptar las Bases de la Promoción.
              Quedarán excluidos de la Promoción quienes no provean todos los
              Datos o no los completen correctamente, quienes no revistan el
              carácter de Comercios Minoristas domiciliados en el Territorio y
              quienes no acepten las Bases de la Promoción. Los participantes
              podrán ingresar una misma Factura en una única oportunidad,
              durante el Plazo de Vigencia.
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
              5. Una vez ingresados correctamente los Datos en el Web Site, los
              participantes (“Participante/s”) participarán automáticamente, con
              una (1) chance por cada Factura que ingresen, en el sorteo
              incluido en la Promoción (“Sorteo”). El Sorteo se llevará a cabo
              en el domicilio de la calle Vuelta de Obligado 1947, 4º piso, “D”,
              Ciudad Autónoma de Buenos Aires, ante Escribano Público, el 22 de
              marzo de 2022 y en él un sistema informático elegirá al azar a
              seis (6) Comercios Minoristas, cuyos titulares (“Potencial/es
              Ganador/es”) podrán ganar: (i) el elegido en primer término, un
              premio en dinero de $ 3.000.000,- (pesos tres millones),
              equivalente al precio de lista, al 27 de diciembre de 2021, del
              automóvil “Renault”, modelo “Kangoo Life SCE”, con motor de 1,6
              litros (“Premio en Dinero”) y (ii) los cinco (5) Participantes
              elegidos del segundo al sexto lugar, cada uno, una (1) tarjeta
              regalo “Visa” del BBVA Argentina (el “Banco”), precargadas con la
              suma de $ 100.000,- (pesos cien mil), cada una ( “Tarjeta/s
              Regalo” y juntamente con el Premio en Dinero, los “Premios” e
              individualmente un “Premio”). En las provincias de Mendoza y Salta
              se asignarán, como máximo, el Premio en Dinero y una (1) Tarjeta
              Regalo, en cada una de ellas.
              <br />
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
              6. Los Premios no incluyen ninguna otra prestación, bien o
              servicio distinto de lo indicado en el punto 5., precedente, el
              derecho a su asignación es intransferible y no podrá requerirse su
              canje por ningún bien o servicio diferente, caso contrario sus
              Potenciales Ganadores perderán automáticamente todo derecho a su
              asignación y no tendrán derecho a reclamo o compensación alguna.
              El Premio en Dinero será entregado mediante cheque cruzado (para
              acreditar en cuenta) librado por el Organizador, a nombre del
              ganador, con cláusula “no a la orden” o mediante transferencia
              bancaria a una cuenta bancaria abierta en la República Argentina a
              nombre del ganador. Las Tarjetas Regalo incorporan un Número de
              Identificación Personal (PIN), que recibirán sus ganadores al
              momento de su entrega. La Tarjeta Regalo no es ni tarjeta de
              crédito, ni tarjeta de débito, ni tarjeta de compra en los
              términos de la ley 25.065 y demás normas aplicables, por lo que no
              se encuentra alcanzada por la legislación aplicable a los
              productos mencionados. Las Tarjetas Regalo no pueden ser
              recargadas, sólo podrán ser utilizadas para pagar la compra de
              bienes o la contratación de servicios dentro del período de
              vigencia que figurará en el anverso de ellas, en los comercios de
              la República Argentina que cuenten con terminales POS y se
              encuentren adheridos al sistema. La Tarjeta Regalo no podrá ser
              utilizada para obtener dinero en efectivo a través de los Cajeros
              Automáticos. Los ganadores de una Tarjeta Regalo podrán consultar
              las operaciones realizadas y el saldo disponible en cualquier
              oficina del Banco, facilitando el número de Tarjeta Regalo y
              previa identificación personal completa o llamando al número
              especialmente asignado para Tarjetas Regalo de Visa Argentina:
              0810-222-7342. En caso de pérdida o sustracción de la Tarjeta
              Regalo notificar al Banco, de inmediato, en cualquiera de las
              oficinas del Banco en horas de atención al público o al teléfono
              (011) 4379-3333 de Visa Argentina. Ni el Organizador ni el Banco
              se harán cargo del monto de reimpresión de la Tarjeta Regalo en
              caso de pérdida o sustracción, teniendo el ganador que hacerse
              cargo de este monto.
              <br />
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
              7. Los Potenciales Ganadores serán notificados de su carácter de
              tales vía mail o telefónicamente y deberán reclamar la asignación
              de los Premios y coordinar la fecha en que contestarán las
              preguntas de cultura general mencionadas en el punto siguiente de
              estas Bases, llamando, dentro de los cinco (5) días de
              notificados, de lunes a viernes, en días hábiles, de 10:00 a 13:00
              horas o de 14:00 a 17:00 horas, al teléfono (011) 5789-7109. En el
              caso de que el Organizador no lograra notificar por mail o por
              teléfono a un Potencial Ganador, a la dirección de correo
              electrónico o al teléfono informados en el Web Site, después de
              realizar al menos dos (2) intentos, separados por un mínimo de
              treinta (30) minutos, ese Potencial Ganador perderá
              automáticamente el derecho a que el Premio le sea asignado. La
              falta de reclamo de la asignación de los Premios en la forma y
              dentro del plazo establecido precedentemente o la contestación
              incorrecta a más de una (1) de las preguntas de cultura general o
              la negativa de los Potenciales Ganadores a presentarse a sesiones
              de fotografía o filmación que eventualmente les requiera el
              Organizador o el incumplimiento de cualquier otro requisito de
              asignación de los Premios establecido en estas Bases les hará
              perder, automáticamente, el derecho a su asignación. Los Premios
              no asignados a ganador alguno, si los hubiere, quedarán en
              propiedad del Organizador. <br />
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
              8. La asignación de los Premios se encuentra asimismo condicionada
              a que sus Potenciales Ganadores contesten correctamente al menos
              cuatro (4) de las cinco (5) preguntas de cultura general que se
              les efectuarán en las fechas previamente acordadas. <br />
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
              9. Las Tarjetas Regalo de los ganadores que se domicilien en la
              Ciudad Autónoma de Buenos Aires o en un radio de setenta (70)
              kilómetros contados desde el centro geográfico de ella serán
              entregadas a sus ganadores en la Ciudad Autónoma de Buenos Aires,
              dentro de los treinta (30) días corridos de la asignación de cada
              una de ellas. El Organizador remitirá por correo las Tarjetas
              Regalo a los domicilios de sus Potenciales Ganadores que residan a
              más de setenta (70) kilómetros de la Ciudad Autónoma de Buenos
              Aires. En caso de que el correo concurriera en más de dos (2)
              oportunidades y un Potencial Ganador no se encontrara disponible
              para exhibir su DNI y entregar una fotocopia de este, contestar
              las preguntas de cultura general, firmar la autorización de uso de
              imagen y recibir el Premio, dicho Potencial Ganador perderá
              automáticamente el derecho a que la Tarjeta Regalo le sea
              asignada. El Premio en Dinero será entregado en la Ciudad Autónoma
              de Buenos Aires, dentro de los treinta (30) días corridos de su
              asignación. <br />
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
              10. Será a cargo de los ganadores todo impuesto, tasa o
              contribución que deba tributarse sobre o en relación con los
              Premios y toda suma de dinero que deba abonarse por cualquier
              concepto al estado nacional, sociedades del estado, provincias,
              municipalidades u otros entes gubernamentales con motivo de la
              organización de la Promoción, de la realización del Sorteo o del
              ofrecimiento de los Premios, con excepción del Impuesto a los
              Concursos, Certámenes, Sorteos y Otros Eventos, reglamentado por
              el artículo 289° y siguientes del Código Fiscal de Mendoza, que
              estará a cargo del Organizador. Los gastos en que incurran los
              Potenciales Ganadores cuando concurran a reclamar la asignación o
              a retirar los Premios estarán asimismo a su exclusivo cargo.
              <br />
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
              11. Los Potenciales Ganadores autorizarán al Organizador, como
              condición para tener derecho a la asignación de los Premios, a
              utilizar sus nombres e imágenes y los de sus Comercios Minoristas,
              con fines comerciales, en los medios de comunicación y formas que
              el Organizador disponga, sin que ello les otorgue derecho a la
              percepción de suma alguna, por ningún concepto, hasta
              transcurridos tres (3) años de la finalización del Plazo de
              Vigencia.
              <br />
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
              12. Sin obligación de compra. A los titulares de Comercios
              Minoristas cuyos comercios estén ubicados en el Territorio, que en
              el Plazo de Vigencia envíen por mail una imagen de un dibujo
              simple, hecho a mano y coloreado, del logo de cualquiera de los
              Productos Participantes (“Dibujo/s”) y copia de la habilitación de
              su Comercio Minorista a la dirección de correo electrónico{" "}
              <a href="mailto:elsabordeganar@gmail.com">
                elsabordeganar@gmail.com
              </a>{" "}
              con el Asunto: “PROMO EL SABOR DE GANAR | WEB | MAYORISTAS”,
              indicando en el mail su nombre y apellido, D.N.I., dirección de
              e-mail, fecha de nacimiento, teléfono y domicilio completo, número
              de CUIT y nombre y domicilio de su Comercio Minorista, el
              Organizador les remitirá, por el mismo medio, dentro de las
              veinticuatro (24) horas hábiles de recibido cada Dibujo, un Código
              que les permitirá participar de la Promoción ingresando el Web
              Site www.elsabordeganar.com.ar y utilizando dicho Código en
              sustitución de la Factura. Se enviará, como máximo, un Código por
              cada Dibujo recibido y por persona, por cada semana del Plazo de
              Vigencia. En ningún caso el Organizador enviará más de un Código
              en un misma semana y a un mismo Participante, aunque envíe más de
              un Dibujo por semana.
              <br />
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
              13. Los Datos Personales de los Participantes serán incluidos en
              una base de datos inscripta en el Registro Nacional de Bases de
              Datos Personales por Mondelez Argentina S.A., para establecer
              perfiles determinados con fines promocionales y comerciales. Al
              momento de facilitar al Organizador sus Datos Personales, los
              Participantes prestan expreso consentimiento para que tales Datos
              Personales pueden ser utilizados por el Organizador con fines
              publicitarios y de marketing en general. El titular de los Datos
              Personales tiene la facultad de ejercer el derecho de acceso a los
              mismos en forma gratuita a intervalos no inferiores a seis (6)
              meses, salvo que se acredite un interés legítimo al efecto
              conforme lo establecido en el artículo 14, inciso 3 de la Ley Nº
              25.326 (Disposición 10/2008, artículo 1º, B.O. 18/09/2008). La
              Agencia de Acceso a la Información Pública, Órgano de Control de
              la Ley Nº 25.326, tiene la atribución de atender las denuncias y
              reclamos que se interpongan con relación al cumplimiento de las
              normas sobre Datos Personales. La información de los Participantes
              será tratada en los términos previstos por la Ley Nacional de
              Protección de Datos Personales Nº 25.326. El titular de los Datos
              Personales podrá solicitar el retiro o bloqueo total o parcial de
              su nombre de la base de datos, enviando un e-mail a{" "}
              <a href="mailto:consultas.ar@mdlz.com.">consultas.ar@mdlz.com.</a>{" "}
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
              14. Está prohibido y será anulado cualquier intento o método de
              participación en la Promoción que se realice por cualquier
              proceso, técnica o mecánica de participación distinta a la
              detallada en estas Bases. La utilización de técnicas de naturaleza
              robótica, repetitiva, automática, programada, mecanizada o similar
              llevará a la anulación de todos los registros realizados por el
              Participante, el que podrá asimismo ser descalificado.
              <br />
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
              15. No podrán participar en la Promoción personas que no revistan
              el carácter de titulares de Comercios Minoristas ni titulares de
              Comercios Minoristas domiciliados fuera del Territorio, los
              empleados del Organizador, de sus agencias de publicidad y
              promociones ni empleados de empresas relacionadas al Organizador
              ni de los Mayoristas Adheridos, los parientes por consanguinidad o
              afinidad de tales empleados
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
              16. Ningún Participante podrá ganar más de un (1) Premio y en caso
              de que, por error, un mismo Participante sea elegido en el Sorteo
              en más de una oportunidad sólo será válida su primera elección
              como Potencial Ganador. La probabilidad de ganar un Premio
              dependerá del total de Participantes que intervengan en el Sorteo
              y de la cantidad de chances con las que participe cada
              Participante. En el supuesto de que participasen en el Sorteo
              24.000 Participantes, con una chance cada uno, la probabilidad de
              cada Participante de resultar favorecido en el Sorteo será de 1 en
              4.000.
              <br />
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
              17. El Organizador no será responsable: (i) por ningún daño o
              perjuicio, de cualquier tipo que fuere, que pudieren sufrir los
              Participantes, ganadores o terceros, en sus personas o bienes, con
              motivo de o con relación a su participación en la Promoción o con
              relación a la utilización del Premio; ni (ii) por fallas en la red
              telefónica, en la red Internet ni por desperfectos técnicos,
              errores humanos o acciones deliberadas de terceros que pudieran
              interrumpir o alterar el normal desarrollo de la Promoción. <br />
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
              18. Para cualquier cuestión judicial que pudiera derivarse de la
              realización de la Promoción, los Participantes y el Organizador se
              someterán a la jurisdicción de los tribunales competentes. <br />
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
              19. El Organizador podrá ampliar la cantidad de premios ofrecidos
              y/o el Plazo de Vigencia. Cuando circunstancias no imputables al
              Organizador o no previstas en estas Bases o que constituyan caso
              fortuito o fuerza mayor lo justifiquen, el Organizador podrá
              suspender, cancelar o modificar la Promoción.
              <br />
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
              20. Estas Bases podrán ser consultadas durante el Plazo de
              Vigencia en el Sitio Web{" "}
              <a href="www.elsabordeganar.com.ar.">
                www.elsabordeganar.com.ar.
              </a>
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
              21. El Organizador no solicitará a los Participantes ni a los
              Potenciales Ganadores ninguna de las siguientes informaciones o
              acciones: datos de tarjeta de crédito o débito, claves bancarias,
              contraseñas, realización de transferencias o depósitos bancarios,
              extracciones de cajeros automáticos o cajas de seguridad, como así
              tampoco el pago de sumas de dinero, compra de bienes o
              contratación de servicios. En caso de dudas o consultas sobre la
              Promoción o notificaciones recibidas, los Participantes pueden
              comunicarse vía correo electrónico a{" "}
              <a href="mailto:elsabordeganar@gmail.com.">
                elsabordeganar@gmail.com.
              </a>
              <br />
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
              22. La participación en la Promoción implica la aceptación de
              estas Bases, así como de las decisiones que adopte el Organizador,
              conforme a derecho, sobre cualquier cuestión no prevista en ellas.{" "}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect((state) => ({}), { changePage })(Condiciones);
