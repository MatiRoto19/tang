import React, { Component } from "react";
import { connect } from 'react-redux';
import {
  Redirect
} from "react-router-dom";
import moment from 'moment';
import step from '../images/step.png';
import brandStep from '../images/brand-step.png';
import stepActive from '../images/step-active.png';
import barraDividir from '../images/puntos_separador.png'
import dots from '../images/dots.png';
import linea from '../images/linea.png';
import top from '../images/top.png';
import right from '../images/right.png';
import lupa from '../images/codigo-04.png';
import cod from '../images/cod.png';
import { Form, Input, Modal, Button, Label,Alert,message,Select,Checkbox,DatePicker } from 'antd';
import ReactDOM from 'react-dom';
import RestClient from '../network/restClient';
import GanadorConsumidorComponent from '../FelicitacionesConsumidor';
import FotoComponent from './Foto'
import MensajeComponent from './Mensaje'
import GanadorComponent from './Ganador'
import RedesComponent from './Rs'
import TagManager from 'react-gtm-module'
import cookies from '../images/terra-03.png';
import actions from './../redux/auth/actions';
import bottom from '../images/bottom-03.png';
import cinta from '../images/cinta-19.png';
import esES from "antd/lib/locale/es_ES";

const Option = Select.Option;
const {changePage} = actions;





class CodigoComponent extends Component {

  constructor(props) {
    super(props);

    this.state = {
      error:undefined,
      loadingCodigo: false,
      vCargarCodigo: true,
      vUploadPhoto: false,
      vRedes: false,
      vMessage: false,
      vGanador: false,
      premio: null,
      activateStep2: stepActive,
      activateStep3: step,
      activateStep4: step,
      file: '',
      imagePreviewUrl: '',
      urlImagen:"",
      productos:[],
    }

  }
  componentDidMount(){
    const tagManagerArgs = {
      gtmId: 'GTM-WSJ4CPF',
      dataLayer:{
        event: 'pageview',
        pageTitle: 'Código',
      }
    }
    // this.loadProductos();
    TagManager.initialize(tagManagerArgs)
    // this.props.changePage("PARTICIPAR");
  }

  loadProductos = () =>{
    RestClient.listRecetasHome().then(response=>{
      this.setState({productos:response.data.productos})
    });

  }
  seeUploadPhoto=()=>{
    this.setState({
      vCargarCodigo: false,
      vUploadPhoto: true,
      activateStep3: stepActive,
      activateStep2: step,
      });
  }



  seeMessage=()=>{
    this.setState({
      vCargarCodigo: false,
      vRedes: false,
      vMessage: true,
      activateStep4: stepActive
      });
  }

  handleSubmit = (e)=>{
    e.preventDefault();
  }



  handleImageChange(e) {
   e.preventDefault();

   let reader = new FileReader();
   let file = e.target.files[0];

   reader.onloadend = () => {
     this.setState({
       file: file,
       imagePreviewUrl: reader.result
     });
   }

   reader.readAsDataURL(file)
 }

 handleCargarCodigo = (e)=> {
  e.preventDefault();
  this.props.form.validateFields(['codigo', 'fecha_vencimiento'],(err, values) => {
     if (!err) {
       //this.seeUploadPhoto()
       this.setState({loadingCodigo:true,error:undefined});
       RestClient.ingresarCodigo({codigo:values.codigo, extra: moment(values.fecha_vencimiento).format('YYYY-MM-DD')}).then(ingresado=>{
        let premio=null;
         if(ingresado.data.premio!=null){
           premio= ingresado.data.premio;
         }
        this.setState({loadingCodigo: false, vCargarCodigo:false, premio}, () => {
          this.redesHandler();
        });
        //   this.setState({loadingCodigo:false,vCargarCodigo:false,vUploadPhoto:true,
        //    activateStep2: step,
        //    activateStep3: stepActive,
        //    premio:premio
        //  });

       }).catch(error=>{
         this.setState({loadingCodigo:false,error:error.message});
       })
     }
   });
  }

  fotoHandler = (preview=null,url=null)=>{
    if(url!=null){
      this.setState({urlImagen:url});
    }
   if(preview!=null){
      this.setState({vUploadPhoto:false,vRedes:true,
      activateStep4: stepActive,
      imagePreviewUrl:preview,
      activateStep3: step})
    } else{
      // this.setState({vUploadPhoto:false})
      // this.redesHandler();
      this.redesHandler();
      // this.setState({vUploadPhoto:false,vRedes:true,
      //   activateStep4: stepActive,
      //   imagePreviewUrl:preview,
      //   activateStep3: step})
   }
  }



  redesHandler = ()=>{
     if(this.state.premio==null){
      this.setState({vUploadPhoto:false, vRedes:false,vMessage:true,
        activateStep4: stepActive,
        activateStep3: step})
    } else{
      this.setState({vUploadPhoto:false, vRedes:false,vGanador:true,
      activateStep4: stepActive,
      activateStep3: step})
    }

  }

  disabledStartDate = startValue => {
    return startValue.isBefore('2021-03-01') || startValue.isAfter('2023-07-31');
  };


  render() {
    const { token } = this.props.profile?this.props.profile:{};
    if (token==null || token==undefined || token==undefined) {
      // return <Redirect to={'/participar/registro'} />;
    }

    const { getFieldDecorator } = this.props.form;


    const { vCargarCodigo, vUploadPhoto, vRedes, vMessage, activateStep2, activateStep3, activateStep4 } = this.state;
    let {imagePreviewUrl} = this.state;
    let $imagePreview = null;
    if (imagePreviewUrl) {
        $imagePreview = (<img id='img-upload' src={imagePreviewUrl} />);
      }
    return (


      <div>

      <div className="container wizard containerInside card" id="contenedorCodigo">
        {/* <div className="row row-center">
            <div className="whiteBox">
                <img src={cinta} className="cintaimg" style={{maxWidth:85}}/>
                <div className="whiteSquare participar">
                <div className="roaster red tit">PARTICIPACIÓN</div>
                </div>
            </div>
        </div> */}


      {this.state.vCargarCodigo &&  <div className="row row-center" id="sheet-cargarCodigo">

        <div className="row">

            <Form onSubmit={this.handleCargarCodigo} hideRequiredMark={true} className="formCodigo">
              <p className="resp txt-dato titCod" >
                Cargá el código
              </p>
              {/* <div style={{textAlign:'center'}}><img style={{marginTop:15, maxWidth:230, marginBottom:20}} src={barraDividir} className="barraDividir"/></div> */}
              {this.state.error && <Alert message={this.state.error} type="error" />}
          <div style={{maxWidth:270, margin:'auto'}}>
            {/* <Form.Item
               label="Marca:"
             >
               {getFieldDecorator('producto', {
                 rules: [
                   {required: true, message: 'Por favor ingresa el código de tu pack',}
                 ],
               })(
                   <Select className="success"
                    onChange={this.handleSelectChange}
                  >
                       {this.state.productos.map(producto => (
                      <Option key={producto.id}>{producto.nombre}</Option>
                  ))}

                  </Select>
               )}
             </Form.Item> */}
             <Form.Item
               label="código de tu pack:"
             >
               {getFieldDecorator('codigo', {
                 rules: [
                   {required: true, message: 'Por favor ingresa el código de tu pack',}
                 ],
               })(
                 <Input className="success" />
               )}
             </Form.Item>

             <div className="lupa">
                 <img className="lupaImg" style={{ width: '100%' }} src={lupa} />
             </div>

             <div className="gris" style={{marginBottom: 20}}>
                    <div className="texGris">
                        ingresá todas las letras y números
    a partir de la letra "l" inclusive
                    </div>
             </div>
             <Form.Item label="Fecha de Vencimiento">
              {getFieldDecorator('fecha_vencimiento', {
                 rules: [
                   {required: true, message: 'Por favor ingresa la fecha de vencimiento',}
                 ],
               })(
                <DatePicker 
                  disabledDate={this.disabledStartDate} 
                  showTime={false} 
                  showToday={false} 
                  format="DD-MM-YYYY" 
                  placeholder="dd-mm-yyyy" 
                  style={{width:'100%'}}
                  locale={esES} />,
              )}
            </Form.Item>
             </div>
             <div className="centrado">
             {/* <Button loading={this.state.loadingCodigo} htmlType="submit" className="btn btn-lg boton-recetas animated rubberBand btn-registrarse">cargar código</Button> */}
             
             </div>
             <div style={{textAlign:'center', marginTop:40}}>
              <Button loading={this.state.loadingCodigo}
                type="primary" className="botonPrimario" htmlType="submit">CARGAR</Button>
                <p className="codigoLabelInferior">Recordá guardar el pack. <br/> En caso de resultar ganador <br/> deberás presentarlo.</p>
            </div>
           </Form>



        </div>
      </div>}

      {/* {this.state.vUploadPhoto &&
        <div className="row row-center" id="sheet-subirFoto">
         <FotoComponent  />
          <GanadorConsumidorComponent fotoHandler={this.fotoHandler} redesHandler={this.redesHandler}/>
        </div>} */}

         {/* {this.state.vRedes &&
        <div className="row row-center" id="sheet-redes">
          <RedesComponent redesHandler={this.redesHandler} state={this.state} />
        </div>} */}


      {this.state.vMessage &&
        <div className="row row-center" id="sheet-participando">

          <MensajeComponent/>
        </div>}

      {this.state.vGanador &&
          <div className="row row-center" id="sheet-ganador">
          <GanadorConsumidorComponent fotoHandler={this.fotoHandler} redesHandler={this.redesHandler}/>
      </div>}
      </div>

          {/* <div className="container-fluid">
            <div className="row justify-content-md-center galleria-home full-w small">

              <div className="box-img-cookiess small">
              </div>

            </div>
        </div> */}
    </div>
  );
  }
}
const CodigoWrapped = Form.create({ name: 'codigo' })(CodigoComponent);
export default connect(
  state => ({
    profile: state.Auth.profile,
  }),
  {changePage}
)(CodigoWrapped);
