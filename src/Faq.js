import React, { Component } from "react";
import cinta from "./images/cinta-19.png";

import TagManager from "react-gtm-module";
import actions from "./redux/auth/actions";
import { connect } from "react-redux";
const { changePage } = actions;
class Faq extends Component {
  componentDidMount() {
    const tagManagerArgs = {
      gtmId: "GTM-WSJ4CPF",
      dataLayer: {
        event: "pageview",
        pageTitle: "Faqs",
      },
    };
    TagManager.initialize(tagManagerArgs);
    window.gtag("event", "conversion", {
      allow_custom_scripts: true,
      send_to: "DC-9238337/mdlzt00/nuevo001+standard",
    });

    this.props.changePage("FAQ");
  }
  render() {
    return (
      <div
        className="container"
        style={{ margin: "50px auto", backgroundColor: "#f39906" }}
      >
        {/* <div className="row row-center pt-2">
              <div className="whiteBox">
                  <img src={cinta} className="cintaimg" style={{maxWidth:85}}/>
                  <div className="whiteSquare participar">
                  <div className="roaster red tit">PREGUNTAS FRECUENTES</div>
                  </div>
              </div>
          </div> */}
        <div className="row faq-txt">
          <div className="row faq mb-4">
            <div className="col-12">
              <div className="faq-title">
                1. ¿En qué consiste la Promoción “EL SABOR DE GANAR ” y cuánto
                tiempo dura?
              </div>
            </div>
            <div className="col-12">
              <div className="faq-description">
                La promoción “EL SABOR DE GANAR” consiste en que, durante su
                desarrollo, las personas mayores de 13 años, que ingresen los
                dígitos que conforman el código alfanumérico a partir de la
                letra “L”, inclusive, que aparece en los empaques de los
                productos participantes (Tang, clight y Verao) de la promoción,
                en la página web{" "}
                <a href="www.sabordeganar.com.ar">www.sabordeganar.com.ar</a> y
                anotando sus nombres, apellidos, teléfono celular, correo
                electrónico, provincia, DNI y aceptando las bases y condiciones,
                tendrán una opción para participar en un sorteo de resolución
                inmediata por una tarjeta regalo de $10.000. La promoción es
                válida a nivel nacional excepto en las Provincias de Río Negro,
                Neuquen y Tierra del Fuego, desde el 15 de Enero hasta el 15 de
                Marzo de 2022.
              </div>
              {/* <div className="faq-description" style={{ marginTop: "10px" }}>
                La promoción es válida a nivel nacional excepto en las
                Provincias de Río Negro, Neuquen y Tierra del Fuego, desde el 15
                de Enero hasta el 15 de Marzo de 2022.
              </div> */}
            </div>
          </div>
          <div className="row faq mb-4">
            <div className="col-12">
              <div className="faq-title">
                2. ¿Cuáles son los premios instantáneos de la promoción?
              </div>
            </div>
            <div className="col-12">
              <div className="faq-description">
                Se pondrán en juego, a lo largo de la vigencia de la promoción,
                sesenta y dos (60) tarjetas regalo “Visa” del BBVA Argentina,
                precargadas con la suma de $ 10.000,- (diez mil), cada una.
              </div>
            </div>
          </div>
          <div class="row faq mb-4">
            <div class="col-12">
              <div class="faq-title">
                3. ¿Qué empaques participan en la promoción?
              </div>
            </div>
            <div class="col-12">
              <div class="faq-description">
                Todos los empaques de los polvos para preparar bebidas
                comercializados por el Organizador bajo la marca “Tang”, Clight
                y Verao, en todos sus sabores y tamaños de envase, tenga o no en
                sus envases publicidad de la Promoción.
              </div>
            </div>
          </div>
          <div class="row faq mb-4">
            <div class="col-12">
              <div class="faq-title">
                4. ¿Quiénes pueden participar de la promoción?
              </div>
            </div>
            <div class="col-12">
              <div class="faq-description">
                Sólo podrán participar de la promoción personas de 13 años a
                más, residentes en Argentina, que no estén domiciliadas en las
                Provincias de Río Negro, Neuquen o Tierra del Fuego.
              </div>
            </div>
          </div>
          <div class="row faq mb-4">
            <div class="col-12">
              <div class="faq-title">
                5. ¿Quiénes no pueden participar de la promoción?
              </div>
            </div>
            <div class="col-12">
              <div class="faq-description">
                No podrán participar en la Promoción personas jurídicas,
                personas humanas menores de 13 años o domiciliadas en las
                provincias de Río Negro, Neuquen o Tierra del Fuego o fuera de
                la República Argentina, los empleados del Organizador, de
                empresas relacionadas al Organizador, los parientes por
                consanguinidad o afinidad de tales empleados hasta el segundo
                grado inclusive ni sus cónyuges.
              </div>
            </div>
          </div>
          <div class="row faq mb-4">
            <div class="col-12">
              <div class="faq-title">
                6. ¿Cómo me entero si gané uno de los premios inmediatos?
              </div>
            </div>
            <div class="col-12">
              <div class="faq-description">
                Los Potenciales Ganadores serán notificados de su carácter de
                tales y del Premio Inmediato potencialmente obtenido, a través
                del Web Site y, serán notificados de su carácter de tales vía
                mail o teléfono que hayan registrado en la web señalada.
              </div>
            </div>
          </div>
          <div class="row faq mb-4">
            <div class="col-12">
              <div class="faq-title">
                7. Si soy potencial ganador de un premio, ¿Qué debo presentar
                para que se me adjudique el premio?
              </div>
            </div>
            <div class="col-12">
              <div class="faq-description">
                Para la adjudicación de un premio, los potenciales ganadores
                deberán presentar, fotocopia del DNI, pack con el número de lote
                ganador, además deberán firmar el recibo de premio y responder
                correctamente al menos cuatro (4) de las cinco (5) preguntas de
                cultura general que se les darán al momento de la entrega del
                premio.
              </div>
            </div>
          </div>
          <div class="row faq mb-4">
            <div class="col-12">
              <div class="faq-title">
                8. ¿Es importante guardar el pack con el número de lote que
                cargue en la web de la promoción?
              </div>
            </div>
            <div class="col-12">
              <div class="faq-description">
                Los participantes deberán guardar el pack con el número de lote
                para reclamar el premio si es que fueron asignados potenciales
                ganadores.
              </div>
            </div>
          </div>
          <div class="row faq mb-4">
            <div class="col-12">
              <div class="faq-title">
                9. ¿Cómo y cuándo puedo canjear mi premio?
              </div>
            </div>
            <div class="col-12">
              <div class="faq-description">
                Los ganadores de los premios serán contactados vía telefónica
                y/o vía correo electrónico para darles los detalles de entrega
                del premio. En el caso de menores de edad, para el retiro
                efectivo de los premios, los ganadores deberán asistir
                acompañados de su padre, madre o tutor, junto con la cédula de
                identidad tanto del menor como del responsable.
                <br />
                <br />
              </div>
            </div>
          </div>
          <div class="row faq mb-4">
            <div class="col-12">
              <div class="faq-title">
                10. ¿Cómo hago para participar sin obligación de compra?
              </div>
            </div>
            <div class="col-12">
              <div class="faq-description">
                Para participar sin obligación de compra tenés que tener 13 años
                o más, residir en el Territorio, y enviar por mail, dentro del
                plazo de vigencia de la promoción, un dibujo simple, hecho a
                mano y coloreado del logo de cualquiera de los productos
                participantes a la dirección de correo electrónico{" "}
                <a href="mailto:Elsabordeganar@gmail.com">
                  Elsabordeganar@gmail.com
                </a>{" "}
                con el Asunto: Promoción “El sabor de Ganar Consumidor final”,
                se les enviara, por el mismo medio, un Código que les permitirá
                participar de la Promoción. Se enviará, como máximo, un (1)
                Código por día, por persona.
              </div>
            </div>
          </div>
          <div class="row faq mb-4">
            <div class="col-12">
              <div class="faq-title">11. Mi código fue usado/es inválido</div>
            </div>
            <div class="col-12">
              <div class="faq-description">
                Envíanos un correo a{" "}
                <a href="consumidores.ar@mdlz.com">consumidores.ar@mdlz.com</a>{" "}
                con la imagen del empaque que muestre el código para poder
                ayudarte y tus datos: nombre, apellido, documento, teléfono,
                correo electrónico y código del empaque.
                <br />
              </div>
            </div>
          </div>
          <div class="row faq mb-4">
            <div class="col-12">
              <div class="faq-title">12. Mi empaque no tiene código</div>
            </div>
            <div class="col-12">
              <div class="faq-description">
                Envíanos un correo a{" "}
                <a href="consumidores.ar@mdlz.com">consumidores.ar@mdlz.com</a>{" "}
                con la imagen del empaque que muestre que no aparece el código
                para poder ayudarte y tus datos: nombre, apellido, documento,
                teléfono y correo electrónico. En el caso de menores de edad,
                para el retiro efectivo de los premios, los ganadores deberán
                asistir acompañados de su padre, madre o tutor, junto con la
                cédula de identidad tanto del menor como del responsable.
                <br />
              </div>
            </div>
          </div>
          <div class="row faq mb-4">
            <div class="col-12">
              <div class="faq-title">
                13. El sistema se cuelga y no puedo ingresar el código.
              </div>
            </div>
            <div class="col-12">
              <div class="faq-description">
                Envíanos un correo a{" "}
                <a href="consumidores.ar@mdlz.com">consumidores.ar@mdlz.com</a>{" "}
                contándonos lo que te ha ocurrido e incluye la imagen del
                empaque que muestre el código y tus datos: nombre, apellido,
                cédula de identidad, teléfono, correo electrónico y código del
                empaque.
                <br />
              </div>
            </div>
          </div>
          <div class="row faq mb-4">
            <div class="col-12">
              <div class="faq-title">
                14. ¿Dónde puedo hacer una queja sobre el producto?
              </div>
            </div>
            <div class="col-12">
              <div class="faq-description">
                Si tienes algún comentario o sugerencia te invitamos a
                comunicarte con nosotros a través del 0 800 333 3333 o
                enviándonos un correo a{" "}
                <a href="mailto:consumidores.ar@mdlz.com">
                  consumidores.ar@mdlz.com
                </a>
                <br />
              </div>
            </div>
          </div>
        </div>

        {/* <div className="container-fluid">
            <div className="row justify-content-md-center galleria-home full-w small">

              <div className="box-img-cookiess small">
              </div>

            </div>
          </div> */}
      </div>
    );
  }
}

export default connect((state) => ({}), { changePage })(Faq);
