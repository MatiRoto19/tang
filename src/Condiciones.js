import React, { Component } from "react";
import TagManager from "react-gtm-module";
import cinta from "./images/cinta-19.png";
import actions from "./redux/auth/actions";
import { connect } from "react-redux";
const { changePage } = actions;
class Condiciones extends Component {
  componentDidMount() {
    const tagManagerArgs = {
      gtmId: "GTM-WSJ4CPF",
      dataLayer: {
        event: "pageview",
      },
    };
    TagManager.initialize(tagManagerArgs);
    this.props.changePage("bases y condiciones");
  }

  render() {
    return (
      <div className="container conds containerInside" style={{margin:"50px auto",backgroundColor:"#f39906"}}>
        <div>
          <div class="col-12">
            <div class="conditions-description">
            1. Participan en la promoción denominada “PROMO EL SABOR DE GANAR | WEB | CONSUMIDORES” (la “Promoción”), organizada por Mondelez Argentina S.A. (el “Organizador”), los polvos para preparar bebidas comercializados por el Organizador bajo las marcas “Tang”, “Clight” y “Verao”, en todos sus sabores y tamaños de envase (“Producto/s Participante/s”), obre o no en sus envases (“Envase/s”) publicidad de la Promoción.
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
            2. La Promoción tendrá vigencia en la República Argentina, excepto en las provincias de Río Negro y Tierra del Fuego, Antártida e Islas del Atlántico Sur (“Territorio”), desde el 15 de enero de 2022 hasta el 15 de marzo de 2022 (el “Plazo de Vigencia”) y se regirá por lo establecido en estas bases y condiciones (las “Bases”). 
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
            3. Las personas interesadas en participar de la Promoción deberán, durante el Plazo de Vigencia, ingresar en el sitio Web www.elsabordeganar.com.ar (el “Web Site”) y, siguiendo las instrucciones allí indicadas, completar: (i) Su nombre y apellido, D.N.I., dirección de e-mail, número de teléfono celular y provincia en la que residan (“Datos Personales”) y (ii) Los dígitos que conforman el código alfanumérico que figura en los Envases de cualquier Producto Participante a partir de la letra “L”, inclusive (“Código/s” y en conjunto con los Datos Personales, los “Datos”). Asimismo, deberán aceptar las Bases de la Promoción e indicar que son mayores de trece (13) años. Quedarán excluidos de la Promoción quienes no completen todos los Datos, quienes no los completen correctamente, los menores de trece (13) años y quienes no acepten las Bases de la Promoción. Los Participantes podrán ingresar un mismo Código un máximo de diez (10) veces en un mismo día y hasta un máximo de veinte (20) veces durante el Plazo de Vigencia.
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
            4. Una vez ingresados correctamente los Datos en el Web Site, los participantes (“Participante/s”) participarán inmediata y automáticamente, con una (1) chance por cada Código que ingresen, en un sorteo de resolución inmediata (“Sorteo/s Inmediato/s”). Los Participantes que suban al Web Site una (1) fotografía de sí mismos en la que estén disfrutando y compartiendo con familiares o amigos uno o más de los Productos Participantes (“Foto/s”), obtendrán una (1) chance adicional de salir favorecidos en el Sorteo. Asimismo, los Participantes que compartan en sus redes sociales (Twitter, Instagram y Facebook; al menos en una de ellas) esa Foto, contarán automáticamente con una (1) chance adicional de salir favorecidos en el Sorteo. Serán descalificadas las Fotos que pudieran ser consideradas, a exclusivo criterio del Organizador, ofensivas, discriminatorias, violatorias de cualquier ley o contrarias a la moral o a las buenas costumbres y las que exhiban marcas de cualquier producto o servicio distintas de las marcas “Tang”, “Clight” o “Verao”. Los Participantes deberán conservar los Envases de los Productos Participantes cuyos Códigos hubiesen utilizado para participar de la Promoción.
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
            5. Mediante los Sorteos Inmediatos se pondrán en juego sesenta (60) tarjetas regalo “Visa” del BBVA Argentina (el “Banco”), precargadas con la suma de $ 10.000,- (pesos diez mil), cada una (“Premio/s” o “Tarjeta/s Regalo”, indistintamente). En las provincias de Neuquén, Mendoza y Salta se asignarán, como máximo, tres (3) Premios, en cada una de ellas.
              <br />
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
            6. Las Tarjetas Regalo incorporan un Número de Identificación Personal (PIN), que recibirán sus ganadores al momento de su entrega. La Tarjeta Regalo no es ni tarjeta de crédito, ni tarjeta de débito, ni tarjeta de compra en los términos de la ley 25.065 y demás normas aplicables, por lo que no se encuentra alcanzada por la legislación aplicable a los productos mencionados. Las Tarjetas Regalo no pueden ser recargadas, sólo podrán ser utilizadas para pagar la compra de bienes o la contratación de servicios dentro del período de vigencia que figurará en el anverso de ellas, en los comercios de la República Argentina que cuenten con terminales POS y se encuentren adheridos al sistema. La Tarjeta Regalo no podrá ser utilizada para obtener dinero en efectivo a través de los Cajeros Automáticos. Los ganadores de una Tarjeta Regalo podrán consultar las operaciones realizadas y el saldo disponible en cualquier oficina del Banco, facilitando el número de Tarjeta Regalo y previa identificación personal completa o llamando al número especialmente asignado para Tarjetas Regalo de Visa Argentina: 0810-222-7342. En caso de pérdida o sustracción de la Tarjeta Regalo notificar al Banco, de inmediato, en cualquiera de las oficinas del Banco en horas de atención al público o al teléfono (011) 4379-3333 de Visa Argentina. Ni el Organizador ni el Banco se harán cargo del monto de reimpresión de la Tarjeta Regalo en caso de pérdida o sustracción, teniendo el ganador que hacerse cargo de este monto.
              <br />
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
            7. La asignación del carácter de potencial ganador de los Sorteos Inmediatos y del Premio asociado estará predeterminada en un programa de computación (“Software”), que contendrá para cada día en que se encuentre vigente la Promoción los horarios ganadores para cada uno de los Premios puestos en juego (“Horario/s Ganador/es”). Cuando un Participante participe en un Sorteo Inmediato en uno de los Horarios Ganadores se constituirá en el potencial ganador del Premio asociado a dicho Horario Ganador (“Potencial/es Ganador/es”). En el caso de que ningún Participante participase de un Sorteo Inmediato en un determinado Horario Ganador, el Potencial Ganador del Premio atribuido a dicho Horario Ganador será el primer Participante que participe en la Promoción con posterioridad al Horario Ganador vacante. Los Horarios Ganadores han sido establecidos ante escribano público con anterioridad al comienzo de la Promoción.      
              <br />
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
            8. Los Potenciales Ganadores de los Premios serán inmediatamente notificados de su condición de tales y del Premio que podrán ganar mediante un aviso automático en pantalla, en el que se incluirá un registro de seguridad (“Notificación/es”), que incluirá un código numérico o alfanumérico (“Código de Autenticación”). Quienes no se constituyan en Potenciales Ganadores serán asimismo notificados a través del Web Site de su condición de no ganadores. <br />
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
            9. Los Premios no incluyen ninguna otra prestación, bien o servicio distinto de lo indicado en el punto 5., precedente, el derecho a su asignación es intransferible y no podrá requerirse su canje por dinero ni por otros bienes o servicios distintos del Premio ofrecido, caso contrario sus Potenciales Ganadores perderán automáticamente todo derecho a su asignación y no tendrán derecho a reclamo o compensación alguna.
          </div>

          <div class="col-12">
            <div class="conditions-description">
            10. Los Potenciales Ganadores deberán reclamar la asignación de los Premios y coordinar la fecha en que contestarán las preguntas de cultura general que les formulará el Organizador, como uno de los requisitos de asignación de los Premios, dentro de los cinco (5) días de recibida la Notificación, de lunes a viernes, en días hábiles, de 10:00 a 13:00 hs. y de 14:00 17:00 hs., llamando al teléfono 5789-7109, y enviar, dentro del mismo plazo, a la dirección electrónica que se les informará, una fotografía de los Envases cuyos Códigos hubiesen utilizado para participar de la Promoción, sin perjuicio de que el Organizador podrá requerir el envío por correo de los Envases originales.
              <br />
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
            11. La falta de reclamo de la asignación de los Premios en la forma y dentro del plazo establecido en el punto precedente o la contestación incorrecta a más de una (1) de las preguntas de cultura general mencionadas en el punto siguiente o la falta de presentación del Envase con el Código o la información de Datos incorrectos o la negativa de los Potenciales Ganadores a presentarse a sesiones de fotografía o filmación que eventualmente les requiera el Organizador o el incumplimiento de cualquier otro requisito de asignación de los Premios establecido en estas Bases les hará perder, automáticamente, el derecho a su asignación. Los Premios no asignados a ganador alguno, si los hubiere, quedarán en propiedad del Organizador. <br />
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
            12. La asignación de los Premios se encuentra asimismo condicionada a que sus Potenciales Ganadores contesten en forma correcta al menos cuatro (4) de las cinco (5) preguntas de cultura general que se les efectuarán en una fecha previamente acordada. <br />
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
            13. Los Premios serán entregados a sus ganadores en la Ciudad Autónoma de Buenos Aires, dentro de los treinta (30) días corridos de la asignación de cada uno de ellos. El Organizador remitirá por correo los Premios a los domicilios de los Potenciales Ganadores que residan a más de setenta (70) kilómetros de la Ciudad Autónoma de Buenos Aires. En caso de que el correo concurriera en más de dos (2) oportunidades y el Potencial Ganador – o, en su caso, el padre, madre o tutor - no se encontrara disponible para exhibir su DNI y entregar una fotocopia del mismo, entregar el Envase con el Código o – en su caso – el Código de participación sin obligación de compra, contestar las preguntas de cultura general, firmar la autorización de uso de imagen y recibir el Premio, dicho Potencial Ganador perderá automáticamente el derecho a que el Premio le sea asignado. Si un Potencial Ganador fuera menor de edad deberá ser representado por su padre, madre, tutor o representante al momento del reclamo, asignación y – eventualmente - entrega del Premio, previa acreditación del vínculo mediante presentación de la documentación correspondiente, de la cual deberá entregarse una fotocopia al Organizador.
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
            14. Será a cargo de los ganadores todo impuesto, tasa o contribución que deba tributarse sobre o en relación con los Premios y toda suma de dinero que deba abonarse por cualquier concepto al estado nacional, sociedades del estado, provincias, municipalidades u otros entes gubernamentales con motivo de la organización de la Promoción, de la realización de los Sorteos Inmediatos o del ofrecimiento de los Premios, con excepción del Impuesto a los Concursos, Certámenes, Sorteos y Otros Eventos, reglamentado por el artículo 289° y siguientes del Código Fiscal de Mendoza, que estará a cargo del Organizador. Los gastos en que incurran los Potenciales Ganadores cuando concurran a reclamar la asignación o a retirar los Premios estarán asimismo a su exclusivo cargo.
              <br />
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
            15. Los Potenciales Ganadores – o, en su caso, sus padres, tutores o representantes legales - autorizarán al Organizador, como condición para tener derecho a la asignación de los Premios, a utilizar sus nombres e imágenes, con fines comerciales, en los medios de comunicación y formas que el Organizador disponga, sin que ello le otorgue derecho a la percepción de suma alguna, por ningún concepto, durante la vigencia de la Promoción y hasta transcurridos tres (3) años de la finalización del Plazo de Vigencia. <br />
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
            16. La asignación de los Premios está asimismo condicionada a la comprobación de la autenticidad de los Envases, de los Códigos de Autenticación y de los Códigos con los que se los reclame. No serán válidos ni se admitirán Envases, Códigos de Autenticación ni Códigos que no sean originales y legítimos. No se aceptarán copias ni reproducciones de los mismos, ni tampoco Envases cuyos Códigos presenten signos de haber sido adulterados, enmendados, alterados, borrados, ilegibles, defectuosos o respecto de las cuales se alegue su pérdida, hurto o robo o que hayan sido obtenidos ilegítimamente, a juicio del Organizador, todos los cuales serán nulos. Es asimismo condición para la asignación de los Premios que los Potenciales Ganadores entreguen los Envases con los Códigos, una fotocopia de su DNI y firmen una autorización para el uso de su imagen. El incumplimiento de cualquiera de estas condiciones hará perder a los Potenciales Ganadores el derecho a la asignación de los Premios, automáticamente, sin derecho a reclamo alguno. 
              <br />
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
            17. Sin obligación de compra. A las personas humanas mayores de 13 años, residentes en el Territorio, que en el Plazo de Vigencia envíen por mail una imagen de un dibujo simple, hecho a mano y coloreado, del logo de cualquiera de los Productos Participantes (“Dibujo/s”) a la dirección de correo electrónico elsabordeganar@gmail.com, con el Asunto: “PROMO EL SABOR DE GANAR | WEB | CONSUMIDORES”, indicando en el mail su nombre y apellido, D.N.I., dirección de e-mail, fecha de nacimiento, teléfono y domicilio completo, el Organizador les remitirá, por el mismo medio, dentro de las veinticuatro (24) horas hábiles de recibido cada Dibujo, un Código que les permitirá participar de la Promoción ingresando el Web Site www.elsabordeganar.com.ar y utilizando dicho Código en sustitución del Código obrante en el Envase de un Producto Participante. Se enviará, como máximo, un Código por cada Dibujo recibido y por persona, por cada día del Plazo de Vigencia. En ningún caso el Organizador enviará más de un Código en un mismo día y a un mismo Participante, aunque envíe más de un Dibujo por día.<br />
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
            18. Los Datos Personales de los Participantes serán incluidos en una base de datos inscripta en el Registro Nacional de Bases de Datos Personales porMondelez Argentina S.A., para establecer perfiles determinados con fines promocionales y comerciales.Al momento de facilitar al Organizador sus Datos Personales, los Participantes prestan expreso consentimiento para que tales Datos Personales pueden ser utilizados por el Organizador con fines publicitarios y de marketing en general. El titular de los Datos Personales tiene la facultad de ejercer el derecho de acceso a los mismos en forma gratuita a intervalos no inferiores a seis (6) meses, salvo que se acredite un interés legítimo al efecto conforme lo establecido en el artículo 14, inciso 3 de la Ley Nº 25.326 (Disposición 10/2008, artículo 1º, B.O. 18/09/2008). La Agencia de Acceso a la Información Pública, Órgano de Control de la Ley Nº 25.326, tiene la atribución de atender las denuncias y reclamos que se interpongan con relación al cumplimiento de las normas sobre Datos Personales. La información de los Participantes será tratada en los términos previstos por la Ley Nacional de Protección de Datos Personales Nº 25.326. El titular de los Datos Personales podrá solicitar el retiro o bloqueo total o parcial de su nombre de la base de datos, enviando un e-mail a <a href="mailto:consultas.ar@mdlz.com.">consultas.ar@mdlz.com.</a> <br />
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
            19. Está prohibido y será anulado cualquier intento o método de participación en la Promoción que se realice por cualquier proceso, técnica o mecánica de participación distinta a la detallada en estas Bases. La utilización de técnicas de naturaleza robótica, repetitiva, automática, programada, mecanizada o similar llevará a la anulación de todos los registros realizados por el Participante, el que podrá asimismo ser descalificado.
              <br />
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
            20. No podrán participar en la Promoción personas jurídicas, personas humanas menores de 13 años o mayores de esa edad domiciliadas fuera del Territorio, los empleados del Organizador, de sus agencias de publicidad y promociones ni empleados de empresas relacionadas al Organizador, los parientes por consanguinidad o afinidad de tales empleados hasta el segundo grado inclusive ni sus cónyuges.
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
            21. La participación en la Promoción implica una declaración jurada de los Participantes en el sentido de que: (i) Son los propietarios exclusivos de las Fotos o poseen las autorizaciones necesarias para enviarlas; (ii) No han cedido a terceros los derechos de publicación o reproducción de las Fotos ni tampoco los derechos de publicación o reproducción de sus nombres o imágenes ni las de los menores de edad que aparezcan en las Fotos; (iii) Cuentan con la autorización de uso de las imágenes de las personas distintas de los Participantes que eventualmente aparezcan en las Fotos y (iv) Las Fotos no violan derechos de terceros de ningún tipo y no han sido obtenidas ilegalmente. El Organizador no asume responsabilidad por las Fotos ni por las consecuencias que su publicación pudiera generar. Los Participantes asumen plena responsabilidad por las Fotos enviadas, liberando al Organizador de toda responsabilidad por cualquier reclamo de terceros, incluyendo derechos de autor, sin que esto implique limitación alguna.
              <br />
            </div>
          </div>

          <div class="col-12">
            <div class="conditions-description">
            22. Los Potenciales Ganadores, como condición para la asignación de los Premios, deberán ceder y transferir al Organizador, sin cargo, la propiedad intelectual de las Fotos, para lo cual suscribirá toda la documentación adicional que pudiera resultar necesaria. El Organizador podrá disponer libre y gratuitamente de las Fotos; las cuales podrá inclusive editar o de cualquier otra forma modificar, musicalizar, utilizar o exhibir, sin necesidad de una autorización adicional, para cualquier propósito y en los medios que el Organizador disponga, sin limitación alguna.
            </div>
          </div>
          <div class="col-12">
            <div class="conditions-description">
            23. Ningún Participante podrá ganar más de un (1) Premio y en caso de que, por error, un mismo Participante sea elegido en más de un Sorteo Inmediato sólo será válida su primera elección como Potencial Ganador. La probabilidad de ganar un Premio dependerá de la cantidad de Premios puestos en juego en un día determinado, del total de Participantes de ese día y de la cantidad de Sorteos Inmediatos en que participe cada Participante. En el supuesto de que en un día determinado del Plazo de Vigencia se pusiese en juego un (1) Premio, un Participante hubiese participado de un (1) Sorteo Inmediato y se hubiesen producido durante dicho día quinientos (500) Sorteos Inmediatos, la probabilidad de tal Participante de resultar favorecido con el Premio será de 1 en 500.  
            </div>
          </div>
          <div class="col-12">
            <div class="conditions-description">
            24. El Organizador no será responsable: (i) por ningún daño o perjuicio, de cualquier tipo que fuere, que pudieren sufrir los Participantes, ganadores o terceros, en sus personas o bienes, con motivo de o con relación a su participación en la Promoción o con relación a la utilización del Premio; ni (ii) por fallas en la red telefónica, en la red Internet ni por desperfectos técnicos, errores humanos o acciones deliberadas de terceros que pudieran interrumpir o alterar el normal desarrollo de la Promoción. 
            </div>
          </div>
          <div class="col-12">
            <div class="conditions-description">
            25. Para cualquier cuestión judicial que pudiera derivarse de la realización de la Promoción, los Participantes y el Organizador se someterán a la jurisdicción de los tribunales competentes.
            </div>
          </div>
          <div class="col-12">
            <div class="conditions-description">
            26. El Organizador podrá ampliar la cantidad de premios ofrecidos y/o el Plazo de Vigencia. Cuando circunstancias no imputables al Organizador o no previstas en estas Bases o que constituyan caso fortuito o fuerza mayor lo justifiquen, el Organizador podrá suspender, cancelar o modificar la Promoción.
            </div>
          </div>
          <div class="col-12">
            <div class="conditions-description">
            27. Estas Bases podrán ser consultadas durante el Plazo de Vigencia en el Sitio Web <a href="www.elsabordeganar.com.ar.">www.elsabordeganar.com.ar.</a>
            </div>
          </div>
          <div class="col-12">
            <div class="conditions-description">
            28. El Organizador no solicitará a los Participantes ni a los Potenciales Ganadores ninguna de las siguientes informaciones o acciones: datos de tarjeta de crédito o débito, claves bancarias, contraseñas, realización de transferencias o depósitos bancarios, extracciones de cajeros automáticos o cajas de seguridad, como así tampoco el pago de sumas de dinero, compra de bienes o contratación de servicios. En caso de dudas o consultas sobre la Promoción o notificaciones recibidas, los Participantes pueden comunicarse vía correo electrónico a elsabordeganar@gmail.com.
            </div>
          </div>
          <div class="col-12">
            <div class="conditions-description">
            29. La participación en la Promoción implica la aceptación de estas Bases, así como de las decisiones que adopte el Organizador, conforme a derecho, sobre cualquier cuestión no prevista en ellas.
            </div>
          </div>
        </div>
      </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({}),
  { changePage }
)(Condiciones);
