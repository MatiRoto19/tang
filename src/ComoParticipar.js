import React, { Component } from "react";
import { Link } from "react-router-dom";
import TagManager from "react-gtm-module";
import cinta from "./images/cinta-19.png";
import cookies from "./images/terra-03.png";
import actions from "./redux/auth/actions";
import { Row, Col, Button } from "antd";
import { connect } from "react-redux";
import barraDividir from "./images/puntos_separador.png";
const { changePage } = actions;

class ComoParticipar extends Component {
  componentDidMount() {
    const tagManagerArgs = {
      gtmId: "GTM-WSJ4CPF",
      dataLayer: {
        event: "pageview",
        pageTitle: "Como participar",
      },
    };
    TagManager.initialize(tagManagerArgs);
    window.gtag("event", "conversion", {
      allow_custom_scripts: true,
      send_to: "DC-9238337/mdlzt00/nuevo00+standard",
    });

    this.props.changePage("Cómo participar");
  }

  render() {
    return (
      <div className="container comoParticipar containerInside">
        {/* <div className="row row-center pt-2">
          <div className="whiteBox">
              <img src={cinta} className="cintaimg" style={{maxWidth:85}}/>
            <div className="whiteSquare participar">
              <div className="roaster red tit">CÓMO PARTICIPAR</div>
            </div>
          </div>
       </div> */}

        <div
          className="row row-center"
          style={{ maxWidth: 600, margin: "auto" }}
        >
          <Row className="itemSeparator card">
            <Col xs={4} className="card card-num comoParticiparNumeros">
              <div className="number">.01</div>
            </Col>
            <Col xs={20} offset={4} className="pos">
              <div className="roaster tit">Cargá tus datos</div>
              <div className="gotham text regularTextSize">
                Completá tus datos personales, recordá que para participar tenes
                que ser mayor de 13 años.
              </div>
            </Col>
          </Row>

          <Row className="itemSeparator card">
            <Col xs={4} className="card card-num comoParticiparNumeros">
              <div className="number">.02</div>
            </Col>
            <Col xs={20} offset={4} className="pos">
              <div className="roaster tit">Cargá el código</div>
              <div className="gotham text regularTextSize">
                Completá el código de tu pack (N° de lote y fecha de vencimiento)
              </div>
            </Col>
          </Row>

          <Row className="itemSeparator card">
            <Col xs={4} className="card card-num comoParticiparNumeros">
              <div className="number">.03</div>
            </Col>
            <Col xs={20} offset={4} className="pos">
              <div className="roaster tit">Pódes ganar $10.000</div>
              <div className="gotham text regularTextSize">
                Todos los días están en juego $10.000 y vos podes ser uno de los
                ganadores.
              </div>
            </Col>
          </Row>

          {/* <Row className="itemSeparator card">
            <Col xs={4}>
              <div className="number">.4</div>
            </Col>
            <Col xs={20} >
              <div className="roaster tit">Triplicá tus chances</div>
              <div className="gotham text regularTextSize">
                Compartí tu foto en Facebook y triplicá chances.
              </div>
            </Col>
          </Row> */}

          {/* <h1 className="roaster yellow lineheight" style={{marginTop:20}}>y ya estás participando<br/>en el sorteo final por<br/>$200.000!</h1> */}
          <div className=" participarBox" style={{ marginTop: 20 }}>
            <Link to="/participar/registro">
              <Button type="primary" className="botonPrimario">
                PARTICIPAR
              </Button>
            </Link>
          </div>
        </div>

        {/* <div className="container-fluid">
          <div className="row justify-content-md-center galleria-home full-w small"> */}

        {/* <div className="box-img-cookiess small">
                <img src={cookies} className="card-img-top small"/>
            </div> */}
        {/* 
          </div>
        </div> */}
      </div>
    );
  }
}

export default connect((state) => ({}), { changePage })(ComoParticipar);
