import React, { Component } from "react";
import { connect } from 'react-redux';
import {
  Redirect, Link,withRouter
} from "react-router-dom";
import { Button } from 'antd';
import ReactDOM from 'react-dom';
import TagManager from 'react-gtm-module';
import barraDividir from '../images/puntos_separador.png'

class MensajeComponent extends Component {

  constructor(props) {
    super(props);
  }
  componentDidMount(){
    this.props.history.push("/participar/codigo/seguiparticipando");
    const tagManagerArgs = {
      gtmId: 'GTM-WSJ4CPF',
      dataLayer:{
        event: 'pageview',
        pageTitle: 'Seguí intentando',
      }
    }
    TagManager.initialize(tagManagerArgs)
  }
  render() {
    return (

      <div className="col">
          <p className="resp txt-dato txt-participando" >
          <span className="textoNaranja">¡SEGUÍ INTENTANDO<br/>CARGANDO MÁS<br/>CÓDIGOS!</span>
          </p>
          {/* <p className="resp txt-dato white-txt int">
            participá con nuevos códigos <br/>para sumar chances en el sorteo final por $200.000
          </p>
          <div style={{textAlign:'center'}}><img style={{marginTop:30, maxWidth:230, marginBottom:30}} src={barraDividir} className="barraDividir"/></div> */}

          <div className="centrado col-2-btn pb-5 btnsMsg" style={{textAlign: 'center'}}>
            <div>
              <Link to={"/participar/registro"}>
                <Button className="botonPrimario botonMensaje" type="primary">CARGAR NUEVO CÓDIGO</Button>
              </Link>
            </div>
            <div style={{marginTop: 20}}>
              <Link to={"/"}>
                <Button className="botonPrimario botonMensaje" type="primary">IR AL INICIO</Button>
              </Link>
            </div>
          </div>

      </div>
    );
  }
}
export default withRouter(MensajeComponent);
