import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Col, Button, Card } from "antd";
// import crea from './images/crea.png';
import banner from "./images/banner.png";
import banner_medio from "./images/contenido-medio.png";
// import instafooter from './images/instafooter.png';
import logo from "./images/logo.png";
import homelogo from "./images/home_logo.png";
import logoMobile from "./images/logoMobile.png";
/* import videoclight from "./videos/videoclight.mp4"; */
import tangIso from "./images/tangiso.png";
import underfooter from "./images/underfooter_1.png";

// import participar from './images/participar.png';
import participarBig from "./images/participar-big-18.png";
import homeBanner from "./images/banner_home.png";
import mobileHomeLabel from "./images/mobileHomeLabel.png";
import menu14 from "./images/menu-14.png";
import menu15 from "./images/menu-15.png";
import menu16 from "./images/menu-16.png";
import menu17 from "./images/menu-17.png";
import menuNew from "./images/menuLong-02.png";
import { Link } from "react-router-dom";

import RestClient from "./network/restClient";
import TagManager from "react-gtm-module";
import actions from "./redux/auth/actions";
const { logout } = actions;

class Home extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    if (this.props.profile) {
      this.props.logout();
    }
    this.props.isHome(true);
  }

  componentWillUnmount() {
    this.props.isHome(false);
  }

  loadRecetas = () => {
    RestClient.listRecetasHome().then((recetasObj) => {
      let recetas = recetasObj.data.recetas;
      recetas.map((receta) => {
        receta.key = `${receta.id_receta}-receta`;
      });
      this.setState({ recetas });
    });
  };

  routeRecetas() {
    window.location = "/#recetas";
  }

  routeParticipar() {
    window.location = "/#participar";
  }

  render() {
    return (
      <>
      <div className="container">
        <div className="row homeTags">
          {/* <div className="col-sm-12 col-12">
          <div className="imgBoxHome">
           <img className="homeLogImg" alt="logo" src={logo}/>
          </div>
        </div> */}
          <Row className="containerLogoHome">
            <Col xs={23} sm={24} md={24} lg={24}>
              <div className="imgBoxHome">
                <img className="homeLogImg efectoLogo " alt="logo" src={logo} />
              </div>
            </Col>
          </Row>
          {/* <Row className="containerMobileLogoHome" style={{display:"none"}}>
            <Col xs={24} sm={24} md={24} lg={24}>
              <div className="imgBoxHome">
                <img className="homeLogImg" alt="logo" src={logo} />
              </div>
            </Col>
          </Row> */}
        </div>
        <div style={{marginTop:"2em",marginBottom:"6em"}}>
            <Row>
              <div className="contenedorBotonesHome">
                <Col xs={19} sm={13} md={12}>
                  <div className="botonesHome">
                    <div className="cuadroBotonPrimario">
                      <p>
                        PROMO <br/>
                        <span className="txts">
                          CONSUMIDOR
                        </span>
                      </p>
                    </div>
                    <div className="homeMenuBtn participarBox">
                      <Link to="/subHome">
                        <Button type="primary" className="botonPrimario">
                          INGRESAR
                        </Button>
                      </Link>
                    </div>
                  </div>  
                </Col>
                <Col xs={19} sm={13} md={12} >
                  <div className="botonesHome2">
                    <div className="cuadroBotonPrimario">
                      <p>
                        PROMO <br/>
                        <span className="txts">
                          COMERCIANTE
                        </span>
                      </p>
                    </div>
                    <div className="homeMenuBtn participarBox">
                      <Link to="/participar/registroComercio">
                        <Button type="primary" className="botonPrimario">
                          INGRESAR
                        </Button>
                      </Link>
                    </div>
                  </div>  
                </Col>
              </div>
            </Row>
        </div>
        
      </div>
      </>
    );
  }
}

export default connect(
  (state) => ({
    profile: state.Auth.profile,
  }),
  { logout }
)(Home);
