import React, { Component } from "react";
import css from "./index.css";
import { Row, Col, Divider,Button } from 'antd';
import { Link } from "react-router-dom";

import premio from "./images/franja10mil-05.png"

const FelicitacionesConsumidor = () =>{
    return(
        <div>
            <div className="wizard containerInside card card-form" style={{margin:"auto", padding:"23px 0"}}>
                <Row>
                    <Col className="gutter-row" xs={24} sm={24} md={24} lg={24}>
                        <img src={premio} width={"100%"} />
                    </Col>
                    <Col className="gutter-row" xs={24} sm={24} md={24} lg={24}>
                        <h2 className="felicitaciones">¡FELICITACIONES!</h2>
                    </Col>
                    <Col className="gutter-row" xs={24} sm={24} md={24} lg={24}>
                        <p className="parrafoPremio">RESULTASTE POTENCIAL<br/>GANADOR DE $10.000</p>
                    </Col>
                    <Col className="gutter-row" xs={24} sm={24} md={24} lg={24}>
                        <p className="parrafoPremioNaranja">COMUNICATE AL 5789-7109<br/>
                        O POR MAIL AL ELSABORDEGANAR@GMAIL.COM<br/>PARA COORDINAR LA ENTREGA DEL PREMIO</p>
                    </Col>
                    <Col className="gutter-row" xs={24} sm={24} md={24} lg={24}>
                        <div className="botonNuevoCodigo">
                            <Link exact to="/participar/codigo" className="linkNuevoCodigo">
                                <Button type="primary" htmlType="submit" className="botonPrimario" id="nuevoCodigo">
                                CARGAR UN NUEVO CÓDIGO
                                </Button>
                            </Link>
                        </div>
                    </Col>
                    <Col className="gutter-row" xs={24} sm={24} md={24} lg={24}>
                        <div className="botonInicio">
                            <Link to="/" className="linkInicio">
                                <Button type="primary" className="botonPrimario" id="nuevoCodigo2">
                                IR AL INICIO
                                </Button>
                            </Link>
                        </div>
                    </Col>                 
                </Row>
            </div>
        </div>
    )

}


export default FelicitacionesConsumidor