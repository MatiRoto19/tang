import React, { Component } from "react";
import css from "./index.css";
import { Row, Col, Divider,Button } from 'antd';
import { Link } from "react-router-dom";

import premio from "./images/franja10mil-05.png"

const SeguiParticipando = () =>{
    return(
        <div style={{margin:"60px 0"}}>
            <div id="contenedorDeSeguiParticipando">
                <Row>
                    <Col className="gutter-row" xs={24} sm={24} md={24} lg={24}>
                        <p className="parrafoSeguiParticipando">
                        ¡SEGUÍ INTENTANDO<br/>CARGANDO MÁS<br/>CÓDIGOS!
                        </p>
                    </Col>
                </Row>
                <Row justify="center">
                    <Col className="gutter-row" xs={24} sm={24} md={24} lg={24}>
                        <div className="botonNuevoCodigo">
                            <Link to="/participar/codigo" className="linkNuevoCodigoSeguiParticipando">
                                <Button type="primary" className="botonPrimario" id="nuevoCodigo">
                                CARGAR UN NUEVO CÓDIGO
                                </Button>
                            </Link>
                        </div>
                    </Col>
                    <Col className="gutter-row" xs={24} sm={24} md={24} lg={24}>
                        <div className="botonInicio">
                            <Link to="/" className="linkInicio">
                                <Button type="primary" className="botonPrimario" id="nuevoCodigo2">
                                IR AL INICIO
                                </Button>
                            </Link>
                        </div>
                    </Col>              
                </Row>
            </div>
        </div>
    )

}


export default SeguiParticipando