import React, { Component } from "react";
import {
  useLocation,
  NavLink,
  HashRouter,
  Link,
  BrowserRouter as Router,
  withRouter,
} from "react-router-dom";
import { Row, Col, Button } from "antd";
import Home from "./Home";
import Contact from "./Contact";
import logo from "./images/logo.png";
import sesion from "./images/sesion.png";
import fb from "./images/fb.png";
import twitter from "./images/twitter.png";
import insta from "./images/insta.png";
import elements from "./images/elements.png";
/* import tangIso from './images/tangiso.png'; */
import tangIso from "./images/tangiso.png";
import underfooter from "./images/underfooter_1.png";
import topwave from "./images/topwave.png";
import brand1 from "./images/marcas/1.png";
import brand2 from "./images/marcas/2.png";
import brand3 from "./images/marcas/3.png";
import brand4 from "./images/marcas/4.png";
import brand5 from "./images/marcas/5.png";
import brand6 from "./images/marcas/6.png";
import brand7 from "./images/marcas/7.png";
import brand8 from "./images/marcas/8.png";
import brand9 from "./images/marcas/9.png";
import brand10 from "./images/marcas/10.png";
import brand11 from "./images/marcas/11.png";
import brand12 from "./images/marcas/12.png";
import brand13 from "./images/marcas/13.png";
import brand14 from "./images/marcas/14.png";
import brand15 from "./images/marcas/15.png";
import brand16 from "./images/marcas/16.png";
import brand17 from "./images/marcas/17.png";
import brand18 from "./images/marcas/18.png";
import brand19 from "./images/marcas/19.png";
import brand20 from "./images/marcas/20.png";
import logosFooter from "./images/logosFooter.png";

class Footer extends Component {
  state = {
    full: true,
  };

  constructor(props) {
    super(props);
  }

  render() {
    let location = window.location.pathname;

    if (
      document.getElementsByClassName("slider-control-centerright").length > 0
    ) {
      document.getElementsByClassName(
        "slider-control-centerright"
      )[0].childNodes[0].innerText = ">";

      var span = document.createElement("span");
      span.className = "glyphicon glyphicon-chevron-right";
      document
        .getElementsByClassName("slider-control-centerright")[0]
        .childNodes[0].appendChild(span);
    }
    if (
      document.getElementsByClassName("slider-control-centerleft").length > 0
    ) {
      document.getElementsByClassName(
        "slider-control-centerleft"
      )[0].childNodes[0].innerText = "<";
    }

    const irArriba = () => {
      window.scrollTo(0);
    };

    return (
      <>
        {/* <img alt="logo" style={{width:'100%'}} src={topwave}/> */}

        <div className="ft-wrapper">
          <div className="container-fluid">
            <div style={{ backgroundColor: "white" }}>
              {location === "/" || (
                <div className="justify-content-center row">
                  <Row className="txtFooter">
                    <Col xs={11} sm={6} md={6}>
                      <Link
                        to={
                          location === "/subHome" ||
                          location === "/como-participar" ||
                          location === "/premios" ||
                          location === "/bases-y-condiciones" ||
                          location === "/faq" ||
                          location === "/participar/registro" ||
                          location === "/participar/codigo" ||
                          location === "/productos" ||
                          location === "/participar/codigo/seguiparticipando" ||
                          location === "/contacto"
                            ? "/productos"
                            : "/productosComercial"
                        }
                        onClick={irArriba}
                      >
                        <p className="txtborder underline">PRODUCTOS PARTICIPANTES</p>
                      </Link>
                    </Col>
                    <Col xs={11} sm={6} md={6}>
                      <Link
                        to={
                          location === "/subHome" ||
                          location === "/como-participar" ||
                          location === "/premios" ||
                          location === "/bases-y-condiciones" ||
                          location === "/participar/registro" ||
                          location === "/participar/codigo" ||
                          location === "/productos" ||
                          location === "/faq" ||
                          location === "/participar/codigo/seguiparticipando" ||
                          location === "/contacto"
                            ? "/faq"
                            : "/faqComercial"
                        }
                        onClick={irArriba}
                      >
                        <p className="txtborder underline">PREGUNTAS FRECUENTAS</p>
                      </Link>
                    </Col>
                    <Col xs={11} sm={6} md={6}>
                      <Link
                        to={
                          location === "/subHome" ||
                          location === "/como-participar" ||
                          location === "/premios" ||
                          location === "/faq" ||
                          location === "/productos" ||
                          location === "/participar/codigo" ||
                          location === "/contacto" ||
                          location === "/participar/codigo/seguiparticipando" ||
                          location === "/bases-y-condiciones" ||
                          location === "/participar/registro"
                            ? "/bases-y-condiciones"
                            : "/bases-y-condiciones-comercial"
                        }
                        onClick={irArriba}
                      >
                        <p className="txtborder underline">BASES Y CONDICIONES</p>
                      </Link>
                    </Col>
                    <Col xs={11} sm={6} md={6}>
                      <Link
                        to={
                          location === "/subHome" ||
                          location === "/como-participar" ||
                          location === "/premios" ||
                          location === "/faq" ||
                          location === "/participar/codigo" ||
                          location === "/productos" ||
                          location === "/bases-y-condiciones" ||
                          location === "/contacto" ||
                          location === "/participar/codigo/seguiparticipando" ||
                          location === "/participar/registro"
                            ? "/contacto"
                            : "/contactoComercio"
                        }
                        onClick={irArriba}
                      >
                        <p className="txtborder borderContact underline">CONTACTO</p>
                      </Link>
                    </Col>
                  </Row>
                </div>
              )}

              <Row className="footerBackground">
                <Col xs={24}>
                  <div className="row mx-auto justify-content-md-center ">
                    <div className="col-sm-24 col-xs-24">
                      <div className="copy">
                        <div>
                          {/*  <img
                                  src={tangIso}
                                  style={{ width: 150, marginBottom: 10 }}
                                /> */}
                        </div>
                        <p>
                          POLVO PARA PREPARAR BEBIDA ANALCOHÓLICA ARTIFICIAL
                          DIETÉTICA DE BAJAS CALORÍAS. LIBRE DE GLUTEN. SIN
                          TACC.
                        </p>
                      </div>
                    </div>
                  </div>
                </Col>
              </Row>
              <Row className="footerBackground">
                <div style={{ textAlign: "center" }}>
                  <Col xs={24}>
                    <img
                      className="imagenFooterMobile"
                      src={logosFooter}
                      style={{ marginBottom: "20px" }}
                    />
                  </Col>
                </div>
              </Row>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default withRouter(Footer);
