import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter, Link } from "react-router-dom";
import { Button } from "antd";
import TagManager from "react-gtm-module";
import actions from "./../redux/auth/actions";
const { changePage } = actions;
class GanadorComponent extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    this.props.history.push("/participar/codigo/ganador");
    const tagManagerArgs = {
      gtmId: "GTM-WSJ4CPF",
      dataLayer: {
        event: "pageview",
        pageTitle: "Ganador",
      },
    };
    TagManager.initialize(tagManagerArgs);
    this.props.changePage("GANADOR");
  }

  render() {
    return (
      <div className="col" id="winnerBox">
        <div className="premio-foto py-4">
          <img className="img-p" src={this.props.premio.image} />
        </div>

        <p className="resp txt-dato ganador-txt">
          ¡FELICITACIONES!
          <br />
          <span
            className="nombrePremio"
            dangerouslySetInnerHTML={{ __html: this.props.premio.premio }}
          ></span>
        </p>

        {/* <p className="resp txt-dato white-txt int">
          Y ADEMÁS YA ESTÁS PARTICIPANDO EN EL SORTEO FINAL POR $100.000
          </p> */}

        <p className="txt-flag my-3">
          <span className="bluetxt">COMUNÍCATE AL 011-5789-7132</span> <br></br>
          PARA COORDINAR LA ENTREGA DEL PREMIO
        </p>

        <div className="centrado col-2-btn pb-5 btnsMsg">
          <div>
            <Link to={"/participar/registro"}>
              <Button className="botonPrimario" type="primary">
                CARGAR NUEVO CÓDIGO
              </Button>
            </Link>
          </div>
          <div style={{ marginTop: 20 }}>
            <Link to={"/"}>
              <Button className="botonPrimario" type="primary">
                INICIO
              </Button>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

//export default withRouter(GanadorComponent);
const GanadorRouter = withRouter(GanadorComponent);
export default connect(
  (state) => ({
    profile: state.Auth.profile,
  }),
  { changePage }
)(GanadorRouter);
