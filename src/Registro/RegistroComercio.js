import React, { Component } from "react";
import step from "../images/step.png";
import brandStep from "../images/brand-step.png";
import stepActive from "../images/step-active.png";
import dots from "../images/dots.png";
import linea from "../images/linea.png";
import top from "../images/top.png";
import right from "../images/right.png";
import {
  Form,
  Input,
  Modal,
  Button,
  Label,
  Alert,
  message,
  Select,
  Checkbox,
  DatePicker,
  Row,
  Col,
} from "antd";
import ReactDOM from "react-dom";
import cookies from "../images/terra-03.png";
import bottom from "../images/bottom-03.png";
import cinta from "../images/cinta-19.png";

import RestClient from "../network/restClient";
import RegistroForm from "./RegistroForm";
import TagManager from "react-gtm-module";
import RegistroFormComercial from "./RegistroFormComercial";
import cardimg from "../images/exclusiva-13.png"
import cardimgMobile from "../images/productosParticipantes2Mobile.png"
const Option = Select.Option;

class RegistroComercioComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      vRegister: true,
      vCargarCodigo: false,
      vUploadPhoto: false,
      vMessage: false,
      activateStep2: step,
      activateStep3: step,
      activateStep4: step,
      file: "",
      imagePreviewUrl: "",
    };
  }
  componentDidMount() {
    const tagManagerArgs = {
      gtmId: "GTM-WSJ4CPF",
      dataLayer: {
        event: "pageview",
        pageTitle: "Participar",
      },
    };
    TagManager.initialize(tagManagerArgs);
  }

  seeCargarCodigo = () => {
    this.setState({
      vRegister: false,
      vCargarCodigo: true,
      vMessage: false,
      activateStep2: stepActive,
      activateStep3: step,
      activateStep4: step,
    });
  };

  seeUploadPhoto = () => {
    this.setState({
      vRegister: false,
      vCargarCodigo: false,
      vUploadPhoto: true,
      activateStep3: stepActive,
    });
  };

  seeMessage = () => {
    this.setState({
      vRegister: false,
      vCargarCodigo: false,
      vUploadPhoto: false,
      vMessage: true,
      activateStep4: stepActive,
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
  };

  handleImageChange(e) {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];

    reader.onloadend = () => {
      this.setState({
        file: file,
        imagePreviewUrl: reader.result,
      });
    };

    reader.readAsDataURL(file);
  }

  routeInicio() {
    window.location = "/";
  }

  render() {




    const { getFieldDecorator } = this.props.form;

    const config = {
      rules: [
        { type: "object", required: true, message: "Selecciona una fecha" },
      ],
    };

    const validateStrings = (rule, value, callback) => {
      if (!/^[a-z]*$/i.test(value)) {
        callback("Sólo letras");
      } else {
        callback();
      }
    };

    const {
      vRegister,
      vCargarCodigo,
      vUploadPhoto,
      vMessage,
      activateStep2,
      activateStep3,
      activateStep4,
    } = this.state;
    let { imagePreviewUrl } = this.state;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = <img id="img-upload" src={imagePreviewUrl} />;
    }
    return (
      <div>
      <div className="container">
        <div className="contenedorImagenesRegistro">
          <div><img  src={cardimg} className="card-imgs"/></div>
          <div><img  src={cardimgMobile} className="card-imgs mobile"/></div>
          
          
        </div>
        <div className="wizard containerInside card card-form" id="formRegistro">
          {/* <div className="row row-center">
            <div className="whiteBox">
                <img src={cinta} className="cintaimg" style={{maxWidth:85}}/>
                <div className="whiteSquare participar">
                <div className="roaster red tit">CONTACTO</div>
                </div>
            </div>
        </div> */}

          <div
            id="sheet-register"
            style={{ display: this.state.vRegister ? "block" : "none" }}
          >
            <RegistroFormComercial />
          </div>
        </div>
    </div>

        {/* <div className="container-fluid">
          <div className="row justify-content-md-center galleria-home full-w small">

            <div className="box-img-cookiess small">
            </div>

          </div>
        </div> */}
      </div>
    );
  }
}
const RegistroWrapped = Form.create({ name: "registroComercio" })(
  RegistroComercioComponent
);
export default RegistroWrapped;
