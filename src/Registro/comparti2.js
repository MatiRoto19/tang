import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FacebookShareButton, FacebookIcon, TwitterShareButton, TwitterIcon, InstapaperIcon, InstapaperShareButton } from 'react-share';
import fb from '../images/redess-28.png';
import insta from '../images/redess-29.png';
import tw from '../images/redess-27.png';
import RestClient from '../network/restClient';

import './SocialShare.scss';
class compartirPost extends Component {
  constructor(props) {
    super(props);
    this.state = {
        shareOpen: "closeShare",
        toggleButtonText: "Share this",
        red:'TWITTER'
    };

    this.shareOpenToggle = this.shareOpenToggle.bind(this);

  }

  saveClick = (values) => {
      let res = {
          id_receta:localStorage.getItem('ph'),
          red_social:values.red
      }
      console.log(res)
      RestClient.saveSocialClick(res).then(recetasObj=>{
        console.log('se guardo el click')
      })

  }

shareOpenToggle() {

    if (this.state.shareOpen==="closeShare") {
        this.setState({
            shareOpen: "openShare",
            toggleButtonText: "Hide sharing options"
        });
    }else {
        this.setState({
            shareOpen: "closeShare",
            toggleButtonText: "Share this"
        });
    }
}


render() {


//URL from current page
const url = window.location.href;
//URL patterns for Social media sites share functionalities
const facebookUrl = `https://www.facebook.com/sharer/sharer.php?u=${url}`;
const twitterUrl = `https://twitter.com/intent/tweet?url=${url}`;

return (
    <div className="socialShareContainer">

        <div className="sharebox">

            <a onClick={() =>this.saveClick({red:'TWITTER'})} href={facebookUrl} target="_blank"> <i className="fa fa-facebook-square"></i></a>
            <a href={twitterUrl} target="_blank"> <i className="fa fa-twitter-square"></i></a>
        </div>
    </div>
);
}
}

// compartirPost.propTypes = {
//   Url: PropTypes.string,
//   Titulo: PropTypes.string,
//   Resumen: PropTypes.string
// };

export default compartirPost;
