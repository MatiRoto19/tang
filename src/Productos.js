import React, { Component } from "react";
import product1 from './images/product-01.png';
import product2 from './images/product-02.png';
import product3 from './images/product-03.png';
import product4 from './images/product-04.png';
import product5 from './images/product-05.png';
import product6 from './images/product-06.png';
import product7 from './images/product-07.png';
import product8 from './images/product-08.png';
import product9 from './images/product-09.png';
import product10 from './images/product-10.png';
import product11 from './images/product-11.png';
import product12 from './images/product-12.png';
import product13 from './images/product-13.png';
import product15 from './images/product-15.png';
import product16 from './images/product-16.png';
import brand1 from './images/brand-07.png';
import terra from './images/brand-08.png';
import brand3 from './images/brand-09.png';
import brand4 from './images/brand-10.png';
import brand5 from './images/brand-11.png';
import brand6 from './images/brand-12.png';
import todos from './images/todos-02.png';
import {Link} from "react-router-dom";
import { Button} from 'antd';
import productosParticipantes from './images/productosParticipantes.png';
import TagManager from 'react-gtm-module'
import actions from './redux/auth/actions';
import { connect } from 'react-redux';
const {changePage} = actions;

class Productos extends Component {

  componentDidMount(){
    const tagManagerArgs = {
      gtmId: 'GTM-WSJ4CPF',
      dataLayer:{
        event: 'pageview',
        pageTitle: 'Productos',
      }
    }
    TagManager.initialize(tagManagerArgs)
        this.props.changePage("PRODUCTOS PARTICIPANTES")

  }
  render() {
    return (
      <div className="container productsBg containerInside">

          {/* <div className="row row-center faq-txt faq">

            <div className="productsBox">
                <img src={todos} className="todos"/>
            </div>
          </div> */}
          <div>
            <div className="imgBoxHomeBanner" style={{maxWidth: 660,paddingTop:55}}>
                <img className="imgBtnMenu participarImg" alt="productosParticipantes" src={productosParticipantes}/>
              </div>
          </div>
          {/* <div className=" participarBox" style={{marginTop: 20,textAlign:'center'}}>
            <Link to="/participar/registro">
              <Button type="primary" className="botonPrimario">PARTICIPAR</Button>
            </Link>
          </div> */}

      </div>
    );
  }
}

export default connect(
  state => ({}),
  {changePage}
)(Productos);
