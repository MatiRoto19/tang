import React, { Component } from "react";
import cinta from "./images/cinta-19.png";

import TagManager from "react-gtm-module";
import actions from "./redux/auth/actions";
import { connect } from "react-redux";
const { changePage } = actions;
class Faq extends Component {
  componentDidMount() {
    const tagManagerArgs = {
      gtmId: "GTM-WSJ4CPF",
      dataLayer: {
        event: "pageview",
        pageTitle: "Faqs",
      },
    };
    TagManager.initialize(tagManagerArgs);
    window.gtag("event", "conversion", {
      allow_custom_scripts: true,
      send_to: "DC-9238337/mdlzt00/nuevo001+standard",
    });

    this.props.changePage("faq");
  }
  render() {
    return (
      <div className="container" style={{margin:"50px auto",backgroundColor:"#f39906"}}>
        {/* <div className="row row-center pt-2">
            <div className="whiteBox">
                <img src={cinta} className="cintaimg" style={{maxWidth:85}}/>
                <div className="whiteSquare participar">
                <div className="roaster red tit">PREGUNTAS FRECUENTES</div>
                </div>
            </div>
        </div> */}
        <div className="row faq-txt">
          <div className="row faq mb-4">
            <div className="col-12">
              <div className="faq-title">
                1.- ¿En qué consiste la Promoción “EL SABOR DE GANAR MAYORISTAS
                y cuánto tiempo dura?
              </div>
            </div>
            <div className="col-12">
              <div className="faq-description">
                La promoción “EL SABOR DE GANAR” consiste en que, durante su
                desarrollo, los comerciantes, ya sean personas fisicas mayores
                de 18 años, o personas juridicas, en ambos casos titulares de
                habilitaciones de comercios minoristas autorizados para la venta
                de alimentos y bebidas, que suban la foto de una factura de
                compra de por lo menos 5 Displays de productos Tang, Cligh o
                Verao, en la página web
                <a href=" www.promoclight.com.ar">
                  <u> www.sabordeganar.com.ar </u>
                </a>{" "}
                promocion comerciantes y anotando su Cuit/Cuil, nombe del
                comercio, nombres, apellidos, teléfono celular, correo
                electrónico, provincia, DNI, y aceptando las bases y
                condiciones, tendrán una opción para participar en los sorteos
                finales del: Valor de un Utiliarrio por $3.000.000 de pesos y 5
                sorteos de $100.000 cada uno. La promoción es válida a nivel
                nacional excepto en las Provincias de Río Negro, Neuquen y
                Tierra del Fuego, desde el 15 de Enero hasta el 15 de Marzo de
                2022.
              </div>
            </div>
          </div>
          <div className="row faq mb-4">
            <div className="col-12">
              <div className="faq-title">
                2.- ¿Cuáles son los premios instantáneos de la promoción?{" "}
              </div>
            </div>
            <div className="col-12">
              <div className="faq-description">
                Se pondrán en juego, a lo largo de la vigencia de la promoción,
                un premio en dinero de $ 3.000.000,- (pesos tres millones),
                equivalente al precio de lista, al 27 de diciembre de 2021, del
                automóvil “Renault”, modelo “Kangoo Life SCE”, con motor de 1,6
                litros (“Premio en Dinero”) y (ii) los cinco (5) Participantes
                elegidos del segundo al sexto lugar, cada uno, una (1) tarjeta
                regalo “Visa” del BBVA Argentina (el “Banco”), precargadas con
                la suma de $ 100.000,- (pesos cien mil), cada una ( “Tarjeta/s
                Regalo” y juntamente con el Premio en Dinero, los “Premios” e
                individualmente un “Premio”). En las provincias de Mendoza y
                Salta se asignarán, como máximo, el Premio en Dinero y una (1)
                Tarjeta Regalo, en cada una de ellas.
              </div>
            </div>
          </div>
          <div class="row faq mb-4">
            <div class="col-12">
              <div class="faq-title">
                3.- ¿Qué empaques participan en la promoción?
              </div>
            </div>
            <div class="col-12">
              <div class="faq-description">
                Todos los Displays de empaques de los polvos para preparar
                bebidas comercializados por el Organizador bajo la marca “Tang”,
                Clight y Verao, en todos sus sabores y tamaños de envase, tenga
                o no en sus envases publicidad de la Promoción.
                <br />
              </div>
            </div>
          </div>
          <div class="row faq mb-4">
            <div class="col-12">
              <div class="faq-title">
                4.- ¿Quiénes pueden participar de la promoción?
              </div>
            </div>
            <div class="col-12">
              <div class="faq-description">
                Sólo podrán participar de la promoción las personas humanas
                mayores de 18 años y las personas jurídicas, en ambos casos, que
                sean titulares de la habilitación municipal de comercios
                minoristas autorizados para la venta de alimentos y bebidas
                ubicados en el Territorio (“Comercio/s Minorista/s”), que no
                estén domiciliadas en las Provincias de Río Negro, Neuquen o
                Tierra del Fuego.
                <br />
              </div>
            </div>
          </div>
          <div class="row faq mb-4">
            <div class="col-12">
              <div class="faq-title">
                5.- Quiénes no pueden participar de la promoción?
              </div>
            </div>
            <div class="col-12">
              <div class="faq-description">
                No podrán participar en la Promoción personas jurídicas,
                personas humanas menores de 18 años o domiciliadas en las
                provincias de Río Negro, Neuquen o Tierra del Fuego o fuera de
                la República Argentina, los empleados del Organizador, de
                empresas relacionadas al Organizador, y los parientes por
                consanguinidad o afinidad de tales empleados hasta el segundo
                grado inclusive ni sus cónyuges. No podrán participar tambien,
                aquellas personas físicas y juridicas, que no sean titulares de
                habilitaciones de comercios minoristas autorizados para la venta
                de alimentos y bebidas.
                <br />
              </div>
            </div>
          </div>
          <div class="row faq mb-4">
            <div class="col-12">
              <div class="faq-title">
                6.- Cuantas facturas de compra de producto puedo subir?{" "}
              </div>
            </div>
            <div class="col-12">
              <div class="faq-description">
                Los participantes podras subir, una vez cada factura que
                contenta la compra de por lo menos 5 displays de Tang, Clight o
                Verao. Por cada factura subida, contarán con una chance para el
                sorteo final. Podrán subir todas las facturas de compra, que
                cumpla con los requisitos de participación, durante la vigencia
                de la promoción.
                <br />
              </div>
            </div>
          </div>
          <div class="row faq mb-4">
            <div class="col-12">
              <div class="faq-title">
                7.- ¿Cómo me entero si gané uno de los premios inmediatos?
              </div>
            </div>
            <div class="col-12">
              <div class="faq-description">
                Los Potenciales Ganadores serán notificados de su carácter de
                tales y del Premio potencialmente obtenido, a través del envio
                de un mail o llamado telefónico que hayan registrado en la web
                señalada.
                <br />
              </div>
            </div>
          </div>
          <div class="row faq mb-4">
            <div class="col-12">
              <div class="faq-title">
                8.- Si soy potencial ganador de un premio, ¿Qué debo presentar
                para que se me adjudique el premio?
              </div>
            </div>
            <div class="col-12">
              <div class="faq-description">
                Para la adjudicación de un premio, los potenciales ganadores
                deberán presentar, fotocopia del DNI, Fotocopia de la
                habilitación municipal del comercio, factura de compra de
                productos participantes ingresada en la web, además deberán
                firmar el recibo de premio y responder correctamente al menos
                cuatro (4) de las cinco (5) preguntas de cultura general que se
                les darán al momento de la entrega del premio.
                <br />
              </div>
            </div>
          </div>
          <div class="row faq mb-4">
            <div class="col-12">
              <div class="faq-title">
                9.-¿Es importante guardar la factura de compra que cargue en la
                web de la promoción?
              </div>
            </div>
            <div class="col-12">
              <div class="faq-description">
                Los participantes deberán guardar factura de compra ingresada en
                la web para reclamar el premio si es que fueron asignados
                potenciales ganadores.
                <br />
                <br />
              </div>
            </div>
          </div>
          <div class="row faq mb-4">
            <div class="col-12">
              <div class="faq-title">
                10.- ¿Cómo y cuándo puedo canjear mi premio?
              </div>
            </div>
            <div class="col-12">
              <div class="faq-description">
                Los ganadores de los premios serán contactados vía telefónica
                y/o vía correo electrónico para darles los detalles de entrega
                del premio.
              </div>
            </div>
          </div>
          <div class="row faq mb-4">
            <div class="col-12">
              <div class="faq-title">
                11.- ¿Cómo hago para participar sin obligación de compra?
              </div>
            </div>
            <div class="col-12">
              <div class="faq-description">
                Para participar sin obligación de compra tenés que tener 18 años
                o más, residir en el Territorio, ser titular de un comercio
                minorista con habilitacion municipal y autorizado para la venta
                de alimentos y bebidas y enviar por mail, dentro del plazo de
                vigencia de la promoción, un dibujo simple, hecho a mano y
                coloreado del logo de cualquiera de los productos participantes
                a la dirección de correo electrónico Elsabordeganar@gmail.com
                con el Asunto: Promoción “El sabor de Ganar Consumidor”, se les
                enviara, por el mismo medio, un Código que les permitirá
                participar de la Promoción. Se enviará, como máximo, un (1)
                Código por cada semana de vigencia de la promoción , por
                persona.
                <br />
              </div>
            </div>
          </div>
          <div class="row faq mb-4">
            <div class="col-12">
              <div class="faq-title">
                12.- ¿Dónde puedo hacer una queja sobre el producto?
              </div>
            </div>
            <div class="col-12">
              <div class="faq-description">
                Si tienes algún comentario o sugerencia te invitamos a
                comunicarte con nosotros a través del 0 800 333 3333 o
                enviándonos un correo a{" "}
                <a href="mailto:consumidores.ar@mdlz.com">
                  consumidores.ar@mdlz.com
                </a>
                <br />
              </div>
            </div>
          </div>
        </div>

        {/* <div className="container-fluid">
          <div className="row justify-content-md-center galleria-home full-w small">

            <div className="box-img-cookiess small">
            </div>

          </div>
        </div> */}
      </div>
    );
  }
}

export default connect((state) => ({}), { changePage })(Faq);
