import React, { Component } from "react";
import contacto from "./images/contacto.png";
import applogo from "./images/applogo.png";
import cinta from "./images/cinta-19.png";
import TagManager from "react-gtm-module";
import cookies from "./images/terra-03.png";
import actions from "./redux/auth/actions";
import { connect } from "react-redux";
import barraDividir from "./images/puntos_separador.png";
const { changePage } = actions;

class Contact extends Component {
  componentDidMount() {
    const tagManagerArgs = {
      gtmId: "GTM-WSJ4CPF",
      dataLayer: {
        event: "pageview",
        pageTitle: "Contacto",
      },
    };
    TagManager.initialize(tagManagerArgs);
    this.props.changePage("contacto");
  }
  render() {
    return (
      <div className="container containerInside">
        {/* <div className="row row-center pt-2">
            <div className="whiteBox">
                <img src={cinta} className="cintaimg" style={{maxWidth:85}}/>
                <div className="whiteSquare participar">
                <div className="roaster red tit">CONTACTO</div>
                </div>
            </div>
        </div> */}
        <div className="whiteBox second contacto card csssa">
          <div className=" big contact cardcontact">
            <p className="resp txt-dato contactoCartel" >
              Para cualquier información <br/> aclaración referente a la promo, <br/>mandar un mail a
            </p>
            {/* <img src={barraDividir} className="barraDividir"/> */}
            <p className="mailContact">consultas.ar@mdlz.com</p>
            {/* <img src={barraDividir} className="barraDividir"/> */}
            <p className="footerContactoText">con asunto "Promo El Sabor De Ganar"</p>
          </div>
        </div>
        {/* <div className="container-fluid">
            <div className="row justify-content-md-center galleria-home full-w small">

              <div className="box-img-cookiess small">
              </div>

            </div>
        </div> */}
      </div>
    );
  }
}

export default connect((state) => ({}), { changePage })(Contact);
