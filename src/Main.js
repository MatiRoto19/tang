import React, { Component } from "react";
import {
  Route,
  NavLink,
  HashRouter,
  BrowserRouter,
  Link,
  Switch,
} from "react-router-dom";
import { connect } from "react-redux";
import Home from "./Home";
import Faq from "./Faq";
import Recetas from "./Recetas";
import ComoParticipar from "./ComoParticipar";
// import ParticiparComponent from "./Participar";
import RegistroComponent from "./Registro/Registro";
import RegistroComercioComponent from "./Registro/RegistroComercio";
import CodigoComponent from "./Registro/Codigo";
import { Icon } from "antd";
import { slide as Menu } from "react-burger-menu";
import RecoverPassword from "./RecoverPassword";
import Productos from "./Productos";
import Condiciones from "./Condiciones";
import Premios from "./Premios";
import RedesComponent from "./Registro/Rs";
import Contact from "./Contact";
import logo from "./images/logo.png";
import sesion from "./images/sesion.png";

import participar from "./images/participar.png";
import participarBig from "./images/participar-big-18.png";
import menu14 from "./images/menu-14.png";
import menu15 from "./images/menu-15.png";
import menu16 from "./images/menu-16.png";
import menu17 from "./images/menu-17.png";

import { Form, Input, Modal, Button, Alert, message } from "antd";
import Footer from "./Footer";
import actions from "./redux/auth/actions";
import RestClient from "./network/restClient";
import Foto from "./Registro/Foto";
import Ganador from "./Registro/Ganador";
import GanadorComercial from "./Registro/GanadorComercial";
import ProductosComerciante from "./ProductosComerciante";
import FaqComercial from "./FaqComercial";
import CondicionesComercial from "./CondicionesComercial";
import InicioConsumidor from "./InicioConsumidor";
import ContactComercio from "./ContactComercio";

const { logout } = actions;
const { SubMenu } = Menu;
class Main extends Component {
  state = {
    loading: false,
    visible: false,
    menuOpen: false,
    isHome: false,
    path: "",
    title: "PARTICIPAR",
    collapsed: false,
  };

  componentDidMount() {
    let w = window.location.pathname;
    if (w == "/") {
      this.setState({ isHome: true, path: w });
    } else {
      this.setState({ isHome: false, path: w });
    }
  }

  componentWillReceiveProps(prevProps) {
    // Typical usage (don't forget to compare props):
    if ((this.state.path = !window.location.pathname)) {
      let w = window.location.pathname;
      if (w == "/") {
        this.setState({ isHome: true, path: w });
      } else {
        this.setState({ isHome: false, path: w });
      }
    }
  }

  changeHome = (isHome) => {
    this.setState({ isHome });
  };

  omitSubFooter = (isHome) => {
    this.setState({ isHome });
  };

  showModal2 = () => {
    this.props.form.setFieldsValue({ emailRecover: "" });
    this.setState({
      visible2: true,
    });
  };

  handleOk = () => {
    this.setState({ loading: true });
    setTimeout(() => {
      this.setState({ loading: false, visible: false });
    }, 3000);
  };

  handleOkRecover = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.setState({ loadingRecover: true, errorRecover: undefined });
        let postParams = {
          mail: values.emailRecover,
        };
        RestClient.recoverPassword(postParams)
          .then((user) => {
            message.success(user.data.message, 10);
            this.setState({ loadingRecover: false, visible: false });
          })
          .catch((error) => {
            this.setState({
              loadingRecover: false,
              errorRecover: error.message,
            });
          });
      }
    });
  };

  handleStateChange(state) {
    this.setState({ menuOpen: state.isOpen });
  }

  handleCancel = () => {
    this.setState({ menuOpen: false });
  };

  handleCancel2 = () => {
    this.setState({ visible2: false });
  };
  toggleCollapsed = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  render() {
    let location = window.location.pathname;

    const ScrollToTop = () => {
      window.scrollTo(0, 0);
      return null;
    };

    const { visible, visible2, loading } = this.state;
    const { getFieldDecorator } = this.props.form;
    let pantalla = window.innerWidth;
    const cerrarModal = () => {
      document.getElementById("react-burger-cross-btn").click();
    };

    return (
      <BrowserRouter>
        <div
          className="container-fluid"
          style={{
            paddingLeft: 0,
            marginLeft: 0,
            paddingRight: 0,
            marginRight: 0,
          }}
        >
          <div className={pantalla > 991 ? "container" : ""}>
            <div className="tittleBox">
              <div
                className="ghostMenu"
                style={{ display: this.state.isHome ? "none" : "flex" }}
              >
                <div className="partBox">
                  {/* <Link  className="participarLink" to="/participar/registro"> */}
                  {/* <img className="menuImg" alt="menuItem" src={participar}/> */}
                  <div
                    className="homeMenuBtn participarBox insideParticiparBox"
                    style={{
                      display:
                        location === "/subHome" ||
                        location === "/premios" ||
                        location === "/faq" ||
                        location === "/como-participar" ||
                        location === "/bases-y-condiciones" ||
                        location === "/productos" ||
                        location === "/contacto" ||
                        location === "/participar/registro"
                          ? "block"
                          : "none",
                    }}
                  >
                    <Link
                      to={
                        location === "/subHome" ||
                        location === "/premios" ||
                        location === "/faq" ||
                        location === "/como-participar" ||
                        location === "/bases-y-condiciones" ||
                        location === "/productos" ||
                        location === "/contacto" ||
                        location === "/participar/registro" ||
                        location === "/participar/codigo"
                          ? "/participar/registro"
                          : "/participar/registroComercio"
                      }
                    >
                      <Button type="primary" className="botonPrimario btn-part">
                        PARTICIPAR
                      </Button>
                    </Link>
                  </div>

                  <div
                    className="homeMenuBtn participarBox insideParticiparBox  "
                    style={{
                      display:
                        location === "/participar/registroComercio" ||
                        location === "/productosComercial" ||
                        location === "/faqComercial" ||
                        location === "/bases-y-condiciones-comercial" ||
                        location === "/contactoComercio" ||
                        location === "/participar/ganador"
                          ? "block"
                          : "none",
                    }}
                  >
                    <Link
                      to={
                        location === "/participar/registroComercio" ||
                        location === "/productosComercial" ||
                        location === "/faqComercial" ||
                        location === "/bases-y-condiciones-comercial" ||
                        location === "/contactoComercio"
                          ? "/participar/registroComercio"
                          : "/participar/registro"
                      }
                    >
                      <Button
                        type="primary"
                        className="botonPrimario"
                        id="botonParaComercios"
                      >
                        PARTICIPAR
                      </Button>
                    </Link>
                  </div>

                  {/* </Link> */}
                </div>

                {location === "/participar/registroComercio" ||
                  location === "/participar/foto" ||
                  location === "/participar/ganador" ||
                  location === "/bases-y-condiciones-comercial" ||
                  location === "/faqComercial" ||
                  location === "/contactoComercio" ||
                  location === "/productosComercial" || (
                    <Menu
                      right
                      isOpen={this.state.menuOpen}
                      onStateChange={(state) => this.handleStateChange(state)}
                    >
                      {/* <div className="homeMenuBtn">
             <Link to="/premios" onClick={this.handleCancel}><img className="imgBtnMenu mini" alt="menuItem" src={menu15}/></Link>
            </div>

            <div className="homeMenuBtn">
                 <Link to="/faq" onClick={this.handleCancel}><img className="imgBtnMenu mini" alt="menuItem" src={menu16}/></Link>
            </div>
            <div className="homeMenuBtn">
                <Link to="/recetas" onClick={this.handleCancel}><img className="imgBtnMenu mini" alt="menuItem" src={menu17}/></Link>
            </div> */}
                      <div className="homeMenuBtn menuMovil">
                        <Link to="/como-participar">
                          <Button
                            type="primary"
                            onClick={cerrarModal}
                            className="botonPrimario"
                          >
                            CÓMO PARTICIPAR
                          </Button>
                        </Link>
                      </div>
                      {location === "/participar/registroComercio" || (
                        <div className="homeMenuBtn menuMovil">
                          <Link to="/premios">
                            <Button
                              type="primary"
                              onClick={cerrarModal}
                              className="botonPrimario"
                            >
                              PREMIOS
                            </Button>
                          </Link>
                        </div>
                      )}
                      {/* <div className="homeMenuBtn menuMovil">
                        <Link to={"/faq"}>
                          <Button
                            type="primary"
                            onClick={cerrarModal}
                            className="botonPrimario"
                          >
                            FAQS
                          </Button>
                        </Link>
                      </div> */}
                      {/* <div className="homeMenuBtn menuMovil">
                <Link to="/recetas">
                  <Button type="primary" className="botonPrimario">MURAL</Button>
                </Link>
            </div> */}
                    </Menu>
                  )}
              </div>

              <div
                className="row menu-app menuDes"
                style={{
                  display: this.state.isHome ? "none" : "flex",
                  marginRight: 0,
                }}
              >
                <div className="">
                  <div className="">
                    <div className="brand-content animated heartBeat">
                      <Link className="navbar-brand" to={"/#"}>
                        <img
                          className="brand-logo efectoLogo"
                          alt="logo"
                          src={logo}
                        />
                      </Link>
                    </div>
                  </div>
                </div>
                {/* <div className="col-sm-8 col-6 pb-3 menucol">
                  <div id="navbar" className="navbar">
                    <ul className="nav navbar-nav">
                      <li className="animated swing liMenu participar"><NavLink  className="b-item b-menu" exact to="/participar/registro">
                        <img className="menuImg" alt="menuItem" src={participar}/>
                      </NavLink></li>
                      <li className="animated swing liMenu"><NavLink className="b-item b-menu" to="/como-participar">
                        <img className="menuImg" alt="menuItem" src={menu14}/>
                      </NavLink></li>
                          <li className="animated swing liMenu"><NavLink className="b-item b-menu" to={{pathname:"/premios"}}>
                        <img className="menuImg" alt="menuItem" src={menu15}/>
                      </NavLink></li>
                            <li className="animated swing liMenu"><NavLink className="b-item b-menu" to="/faq">
                        <img className="menuImg" alt="menuItem" src={menu16}/>
                      </NavLink></li>
                            <li className="animated swing liMenu"><NavLink className="b-item b-menu" to="/recetas">
                        <img className="menuImg" alt="menuItem" src={menu17}/>
                      </NavLink></li>
                    </ul>
                  </div>
                  </div> */}
              </div>

              <div
                className="tittle"
                style={{
                  display:
                    this.state.isHome || location === "/subHome"
                      ? "none"
                      : "flex",
                  maxWidth:
                    location === "/productos" ||
                    location === "/productosComercial"
                      ? "390px"
                      : "600px",
                }}
              >
                <h1>{this.props.pageName}</h1>
              </div>
            </div>
          </div>

          <div className="content">
            <Switch component={ScrollToTop}>
              <Route
                exact
                path="/"
                render={(props) => <Home {...props} isHome={this.changeHome} />}
              />
              <Route exact path="/subHome" component={InicioConsumidor} />
              <Route exact path="/recetas" component={Recetas} />
              <Route path="/como-participar" component={ComoParticipar} />
              <Route
                exact
                path="/participar/registro"
                component={RegistroComponent}
              />
              <Route
                exact
                path="/participar/registroComercio"
                component={RegistroComercioComponent}
              />
              <Route path="/participar/codigo" component={CodigoComponent} />
              <Route path="/premios" component={Premios} />
              <Route path="/redes" component={RedesComponent} />

              <Route exact path="/faq" component={Faq} />
              <Route exact path="/faqComercial" component={FaqComercial} />

              <Route path="/productos" component={Productos} />
              <Route
                path="/productosComercial"
                component={ProductosComerciante}
              />

              <Route
                exact
                path="/bases-y-condiciones"
                component={Condiciones}
              />
              <Route
                exact
                path="/bases-y-condiciones-comercial"
                component={CondicionesComercial}
              />

              <Route path="/contacto" component={Contact} />
              <Route path="/contactoComercio" component={ContactComercio} />
              <Route exact path="/participar/foto" component={Foto} />
              <Route path="/participar/ganador" component={GanadorComercial} />
              {/* <Route exact path="/Participar" component={ParticiparComponent}/> */}
              <Route path="/recover-password" component={RecoverPassword} />
            </Switch>
          </div>

          <Footer></Footer>

          <Modal
            visible={visible2}
            title="Olvidé mi contraseña"
            centered
            onCancel={this.handleCancel2}
            footer={null}
          >
            <div className="centrado">
              <div>Te enviaremos un mensaje a tu correo</div>
              <Form layout="vertical" onSubmit={this.handleOkRecover}>
                {this.state.errorRecover && (
                  <Alert message={this.state.errorRecover} type="error" />
                )}
                <Form.Item>
                  {getFieldDecorator("emailRecover", {
                    rules: [
                      { type: "email", message: "El correo no es válido" },
                      {
                        required: true,
                        message: "Por favor ingresa tu correo",
                      },
                    ],
                  })(
                    <Input className="success" placeholder="Escribe tu mail" />
                  )}
                </Form.Item>
                <Form.Item>
                  <Button
                    loading={this.state.loadingRecover}
                    htmlType="submit"
                    className="btn btn-lg boton-recetas modal-btn"
                    size="large"
                  >
                    Enviar
                  </Button>
                </Form.Item>
              </Form>
            </div>
          </Modal>
        </div>
      </BrowserRouter>
    );
  }
}
// <a className="forgot-ses" onClick={this.showModal2}>
// Olvidé mi contraseña</a>
const WrappedMain = Form.create({ name: "recover" })(Main);

export default connect(
  (state) => ({
    profile: state.Auth.profile,
    pageName: state.Auth.pagename,
  }),
  { logout }
)(WrappedMain);
