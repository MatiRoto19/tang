import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FacebookShareButton, FacebookIcon, TwitterShareButton, TwitterIcon, InstapaperIcon, InstapaperShareButton } from 'react-share';
import fb from '../images/redess-28.png';
import insta from '../images/redess-29.png';
import tw from '../images/redess-27.png';

import RestClient from '../network/restClient';
class compartirPost extends Component {

    saveClick = (values) => {
        let res = {
            id_receta:localStorage.getItem('ph'),
            red_social:values.red
        }
        console.log(res)
        RestClient.saveSocialClick(res).then(recetasObj=>{
          console.log('se guardo el click')
        })


    }



  render() {
    console.log(this.props);
    return (
		<div className="colIcons">
		   {/* <TwitterShareButton beforeOnClick={() =>this.saveClick({red:'TWITTER'})} url={this.props.Url} title={this.props.Titulo}>
		   <img className="img-sociali" src={tw}/>
        </TwitterShareButton> */}
        <FacebookShareButton beforeOnClick={() =>this.saveClick({red:'FACEBOOK'})} url={this.props.Url} quote={`${this.props.Titulo} - ${this.props.Resumen}`} >
		      <img className="img-sociali" src={fb}/>
        </FacebookShareButton>
      </div>
    );
  }
}

compartirPost.propTypes = {
  Url: PropTypes.string,
  Titulo: PropTypes.string,
  Resumen: PropTypes.string
};

export default compartirPost;
