import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter ,Link, Redirect} from "react-router-dom";
import {
  Form,
  Input,
  Modal,
  Button,
  Label,
  Alert,
  message,
  Upload,
  Icon,
  Progress,
  Row,
  Col,
} from "antd";
import ReactDOM from "react-dom";
import barraDividir from "../images/puntos_separador.png";
import TagManager from "react-gtm-module";
import cookies from "../images/terra-03.png";
import actions from "./../redux/auth/actions";
import bottom from "../images/bottom-03.png";
import cinta from "../images/cinta-19.png";

import RestClient from "../network/restClient";

const { changePage, logout } = actions;

const { Dragger } = Upload;

const props = {
  name: "file",
  multiple: true,
  action: "https://www.mocky.io/v2/5cc8019d300000980a055e76",
  onChange(info) {
    const { status } = info.file;
    if (status !== "uploading") {
      console.log(info.file, info.fileList);
    }
    if (status === "done") {
      message.success(`${info.file.name} file uploaded successfully.`);
    } else if (status === "error") {
      message.error(`${info.file.name} file upload failed.`);
    }
  },
};

class FotoComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      error: undefined,
      loading: false,
      file: "",
      imagePreviewUrl: undefined,
      imgName: "",
      progreso: 0,
      previewImage: "",
      urlImagen: "",
      previewVisible: false,
      fileList: [],
    };
  }
  componentDidMount() {
    this.props.history.push("/participar/foto");
    const tagManagerArgs = {
      gtmId: "GTM-WSJ4CPF",
      dataLayer: {
        event: "pageview",
        pageTitle: "Foto",
      },
    };
    TagManager.initialize(tagManagerArgs);
    // this.props.changePage("PARTICIPAR");
  }

  handleUploadSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        let fotoObj = {
          image: values.image.file.name,
          receta: values.receta,
        };
        console.log(fotoObj.image);
        this.setState({ loading: true, error: undefined });
        RestClient.preSignedUrl({ filename: fotoObj.image })
          .then((urlObj) => {
            console.log(urlObj);
            let url = urlObj.data.url;
            let urlreal = urlObj.data.urlReal;
            this.setState({ urlImagen: urlreal });
            console.log(url);
            const formData = new FormData();
            const file = this.state.fileList[0];
            formData.append("file", file);
            this.uploadFileToS3(formData, url, file.name, file, fotoObj);
          })
          .catch((error) => {
            console.log(error);
            this.setState({ loading: false, error: error.message });
          });
      }
    });
  };

  uploadFileToS3(postData, url, filename, file, postParams) {
    var xhr = new XMLHttpRequest();
    var secuencia = 0;
    xhr.open("PUT", url);
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
        if (xhr.status === 200 || xhr.status === 204) {
          this.saveRecerta(postParams);
        } else {
          console.log("Error al subir la foto en el servidor");
          this.setState({
            loading: false,
            error: "Error al subir la foto en el servidor",
          });
        }
      }
    }.bind(this);
    xhr.upload.onprogress = function (evt) {
      var percentComplete = (evt.loaded / evt.total) * 100;
      this.setState({ progreso: parseInt(percentComplete) });
    }.bind(this);
    xhr.send(file);
  }

  saveRecerta = (params) => {
    console.log("guardando");
    RestClient.guardarReceta(params)
      .then((guardado) => {
        // localStorage.setItem("ph", guardado.data);
        this.setState({ loading: false, error: undefined });
        if (this.props.fotoHandler) {
          return this.props.fotoHandler(this.state.previewImage, this.state.urlImagen);
        }
        this.props.history.push('/participar/ganador');
      })
      .catch((error) => {
        this.setState({ loading: false, error: error.message });
      });
  };

  getBase64 = (img, callback) => {
    const reader = new FileReader();
    reader.addEventListener("load", () => callback(reader.result));
    reader.readAsDataURL(img);
  };
  handlePreview = async (file) => {
    console.log("preview", file);
    if (!file.url && !file.preview) {
      file.preview = await this.getBase64(file.originFileObj);
    }
    this.setState({
      previewImage: file.url || file.preview,
      previewVisible: true,
    });
  };
  beforeUpload = (file) => {
    console.log(file);
    const isValid = file.type === "image/jpeg" || file.type === "image/png";
    if (!isValid) {
      message.error("Solo se puede subir imágenes");
    }
    const isLt2M = file.size / 1024 / 1024 < 10;
    if (!isLt2M) {
      message.error("La imagen no puede pesar más de 10 mb");
    }
    if (isValid && isLt2M) {
      this.getBase64(file, (imageUrl) => {
        file.thumbUrl = imageUrl;
        console.log(file);
        this.setState({
          fileList: [file],
          imagePreviewUrl: imageUrl,
          previewImage: imageUrl,
          previewVisible: false,
          imgName: file.name,
        });
      });
    }
    return false;
  };
  handleCancel = () => this.setState({ previewVisible: false });
  handleChange = ({ fileList }) => this.setState({ fileList });
  render() {
    const { getFieldDecorator } = this.props.form;
    const { previewVisible, previewImage, fileList, imagePreviewUrl, imgName } =
      this.state;
    const { token } = this.props.profile ? this.props.profile : {};
    if (!token) {
      return <Redirect to={"/participar/registroComercio"} />;
    }
    return (
      <div className="container fotoContainer containerInside card" style={{height:'770px'}}>
        {/* <img src={cinta} className="cintaimg big wizard"/> */}

        <div className="row row-center d-flex justify-content-center">
          <div className="col-lg-12">
            <p
              className="resp txt-dato titCod"
              style={{ marginTop: 10, marginBottom: 20 }}
            >
              {/* <div className="number numberCenter">3.</div> */}
              SUBÍ LA FOTO <br/> DE TU FACTURA
              <br/><small className="yellow">para completar tu  participación</small>
            </p>
            {/* <div style={{textAlign:'center'}}><img style={{marginTop:15, maxWidth:230, marginBottom:20}} src={barraDividir} className="barraDividir"/></div> */}

            <Form onSubmit={this.handleUploadSubmit}>
              <div className="caja-b pt-3 foto-box">
                <div className="form-group caja-img">
                  <div
                    className="input-group borde-blanco"
                    style={{ display: "block" }}
                  >
                    <div className="input-group-btn" style={{height: '100%', overflow: 'hidden'}}>
                      <p
                        className="ant-upload-text"
                        style={{ visibility: previewVisible, height: '100%' }}
                      >
                        {previewImage === "" && "vista previa"}
                        <img
                          alt=""
                          style={{ width: '100%', visibility: previewVisible }}
                          src={previewImage}
                        />
                      </p>
                    </div>
                  </div>
                </div>
                <div className="inputUploadFile">
                  <Form.Item>
                    {getFieldDecorator("image", {
                      rules: [
                        {
                          required: true,
                          message: "Es necesario subir una imagen",
                        },
                      ],
                    })(
                      <Dragger
                        {...props}
                        fileList={fileList}
                        beforeUpload={this.beforeUpload}
                        onPreview={this.handlePreview}
                        onChange={this.handleChange}
                      >
                        <Row>
                          <Col xs={24} sm={24} md={16} lg={16} >
                            <Button
                              type="primary"
                              className="botonPrimario"
                              id="botonDeFoto"
                              style={{ fontSize: 22,padding:"0px 5px" }}
                            >
                              Seleccionar imagen
                            </Button>
                          </Col>
                          <Col xs={24} sm={24} md={16} lg={16} >
                            <span className="fileNameTxtUpload">{imgName}</span> 
                          </Col>
                        </Row>
                        <div className="uploadFileName" style={{display:'inline-flex'}}>
                          {/* <div className="upBtn boton-recetas"</div> */}
                          {/* <span className="fileNameTxtUpload">{imgName}</span> */}
                        </div>
                      </Dragger>
                    )}
                  </Form.Item>
                  <Modal
                    visible={previewVisible}
                    footer={null}
                    onCancel={this.handleCancel}
                  >
                    <img
                      alt="example"
                      style={{ width: "100%" }}
                      src={previewImage}
                    />
                  </Modal>
                </div>
                <div className="centrado pt-3 pb-3 progressUpload">
                  <Row gutter={10}>
                    <div className="contenedorBotonesFoto">
                    <Col xs={24} sm={12} md={10} lg={10}>
                    {/* <Link to="/participar/registroComercio"> */}
                      <Button
                        onClick={() => {
                          this.props.logout();
                        }}
                        className="botonPrimario boton100 btnVolver" 
                        type="primary"
                      >
                        VOLVER
                      </Button>
                    {/* </Link> */}
                    </Col>
                    <Col xs={24}  sm={12}>
                      <Button
                        // onClick={() => this.props.fotoHandler()}
                        htmlType="submit"
                        loading={this.state.loading}
                        htmlType="submit"
                        className="botonPrimario boton100 btnSubirFact"
                        type="primary"
                      >
                        SUBIR FACTURA
                      </Button>
                    </Col>
                    </div>
                  </Row>
                  {this.state.loading && (
                    <div className="contenedorProgress">
                      <Progress percent={this.state.progreso} />
                    </div>
                  )}
                </div>
              </div>
            </Form>
          </div>
        </div>
      </div>
    );
  }
}
const FotoWrapped = Form.create({ name: "foto" })(FotoComponent);
const FotoRouter = withRouter(FotoWrapped);
export default connect(
  (state) => ({
    profile: state.Auth.profile,
  }),
  { changePage, logout }
)(FotoRouter);
